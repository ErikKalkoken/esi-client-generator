from pprint import pprint

import esi_client
from esi_client.api.character_api import CharacterApi

# Defining the host is optional and defaults to https://esi.evetech.net/latest
# See configuration.py for a list of all supported configuration parameters.
configuration = esi_client.Configuration(host="https://esi.evetech.net/latest")

# Enter a context with an instance of the API client
with esi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = CharacterApi(api_client)

    try:
        api_response = api_instance.get_characters_character_id(character_id=93330670)
        pprint(api_response)
    except esi_client.ApiException as ex:
        print(f"Exception occurred when trying to fetch a character: {ex}")
