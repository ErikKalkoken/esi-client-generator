"""Fetch a character from ESI."""
from esi_client.api.character_api import CharacterApi

api = CharacterApi()
character = api.get_characters_character_id(character_id=93330670)
print(character.name)
