import esi_client
from esi_client.api.character_api import CharacterApi
from esi_client.api.corporation_api import CorporationApi

with esi_client.ApiClient() as api_client:
    character_api = CharacterApi(api_client)
    character = character_api.get_characters_character_id(character_id=93330670)
    corporation_api = CorporationApi(api_client)
    corporation = corporation_api.get_corporations_corporation_id(
        corporation_id=character.corporation_id
    )
    print(f"{character.name} if a member of {corporation.name}")
