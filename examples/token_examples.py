from pprint import pprint

import esi_client
from esi_client.api.character_api import CharacterApi

# Variant 1: access token can be provided with the configuration
configuration = esi_client.Configuration(access_token="XXX")

with esi_client.ApiClient(configuration) as api_client:
    api_instance = CharacterApi(api_client)
    api_response = api_instance.get_characters_character_id_medals(
        character_id=93330670
    )
    pprint(api_response)

# Variant 2: access token can be provided as argument when calling the endpoint
with esi_client.ApiClient() as api_client:
    api_instance = CharacterApi(api_client)
    api_response = api_instance.get_characters_character_id_medals(
        character_id=93330670, token="XXX"
    )
    pprint(api_response)
