"""Render project files.

This script allows rendering project files like the README with dynamic information.
"""

import argparse
import json
import urllib.request
from pathlib import Path
from string import Template


def main():
    args = parse_arguments()
    render_readme(args)
    render_pyproject(args)


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("source", help="Path to template folder")
    parser.add_argument(
        "target", help="Path to where the rendered files should be created"
    )
    parser.add_argument("--project-name", required=True)
    parser.add_argument("--description", required=True)
    parser.add_argument("--spec-url", required=True)
    parser.add_argument("--homepage-url", required=True)
    args = parser.parse_args()
    return args


def render_readme(args):
    esi_version = fetch_esi_version(args.spec_url)
    openapigen_version = fetch_openapigen_version(args.project_name)
    context = {
        "esi_version": esi_version,
        "openapigen_version": openapigen_version,
        "project_name": args.project_name,
        "description": args.description,
    }
    render_file(args, "README_template.md", "README.md", context)


def render_pyproject(args):
    context = {
        "project_name": args.project_name,
        "description": args.description,
        "homepage_url": args.homepage_url,
    }
    render_file(args, "pyproject_template.toml", "pyproject.toml", context)


def fetch_esi_version(spec_url: str) -> str:
    print(f"Fetching ESI spec version from: {spec_url}")
    with urllib.request.urlopen(spec_url) as url:
        data = json.load(url)
        return data["info"]["version"]


def fetch_openapigen_version(project_name: str) -> str:
    path = Path(project_name) / ".openapi-generator" / "VERSION"
    with path.open("r") as fp:
        version = fp.read()
    return version.replace("-", "_").strip()


def render_file(args, source_file: str, target_file: str, context: dict):
    source_path = Path(args.source) / source_file
    source = read_source_file(source_path)
    updated = source.substitute(context)
    target_path = Path(args.target) / target_file
    write_target_file(target_path, updated)


def read_source_file(path):
    print(f"Reading source file: {path}")
    with path.open("r") as fp:
        source = Template(fp.read())
    return source


def write_target_file(path, updated):
    print(f"Writing destination file: {path}")
    with path.open("w") as fp:
        fp.write(updated)


if __name__ == "__main__":
    main()
