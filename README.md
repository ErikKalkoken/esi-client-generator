# esi-client-generator

## Overview

This project generates a Python client for accessing the complete Eve Online ESI API. ESI is the public API for the sci-fi MMORG Eve Online.

The Python client is automatically generated from the public ESI spec by the [Open API generator](https://openapi-generator.tech/) and has some some improvements (e.g. the client package is called `esi_client` instead of the generic `openapi_client`).

## ESI client

The newest version of the ready-to-use ESI client can be installed directly from PyPI:

```sh
pip install esi-client
```

For more information about the client please see the [ESI Client README](esi-client/README.md)

## Technical details

>**Note**:<br>This section is for maintainers of this project or those who want to generate their own client.

The client generation is controlled via the makefile, which also is the central configuration source (e.g. current version).

To generate your own client :

1. Clone this repo on your local Linux machine.
2. Make sure you the docker engine installed.

Then you can create a new client with these commands:

```sh
make clean
make generate
```

>**Hint**:<br> If you want to distribute a new version to PyPI you also need to update the version,
which is a parameter in the makefile.
