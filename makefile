project_name = esi-client
package_name = esi_client
version = 0.1.0
homepage_url = https://gitlab.com/ErikKalkoken/esi-client-generator
spec_url = https://esi.evetech.net/latest/swagger.json
description = "A Python client for accessing the complete Eve Online ESI API."
user_id := $(shell id -u)

generate:
	sudo docker run --rm -v "${PWD}:/local" openapitools/openapi-generator-cli generate \
		--generator-name python-prior \
		--input-spec $(spec_url) \
		--output /local/$(project_name) \
		--package-name $(package_name) \
		--additional-properties=projectName="$(project_name),packageVersion=$(version),generateSourceCodeOnly=true"
	sudo chown -R ${user_id}:${user_id} $(project_name)
	cp project_files/* $(project_name)
	python scripts/render_project_files.py templates $(project_name) \
		--project-name $(project_name) \
		--spec-url $(spec_url) \
		--description $(description) \
		--homepage-url $(homepage_url)

clean:
	sudo rm -rf $(project_name)

test:
	cd $(project_name) && tox -e py311

build:
	rm -rf $(project_name)/dist
	cd $(project_name) && python -m build

upload_test:
	cd $(project_name) && twine upload --repository testpypi dist/*

upload:
	cd $(project_name) && twine upload dist/*