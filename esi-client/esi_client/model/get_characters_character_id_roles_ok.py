"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from esi_client.model_utils import (  # noqa: F401
    ApiTypeError,
    ModelComposed,
    ModelNormal,
    ModelSimple,
    cached_property,
    change_keys_js_to_python,
    convert_js_args_to_python_args,
    date,
    datetime,
    file_type,
    none_type,
    validate_get_composed_info,
    OpenApiModel
)
from esi_client.exceptions import ApiAttributeError



class GetCharactersCharacterIdRolesOk(ModelNormal):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.

    Attributes:
      allowed_values (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          with a capitalized key describing the allowed value and an allowed
          value. These dicts store the allowed enum values.
      attribute_map (dict): The key is attribute name
          and the value is json key in definition.
      discriminator_value_class_map (dict): A dict to go from the discriminator
          variable value to the discriminator class name.
      validations (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          that stores validations for max_length, min_length, max_items,
          min_items, exclusive_maximum, inclusive_maximum, exclusive_minimum,
          inclusive_minimum, and regex.
      additional_properties_type (tuple): A tuple of classes accepted
          as additional properties values.
    """

    allowed_values = {
        ('roles',): {
            'ACCOUNT_TAKE_1': "Account_Take_1",
            'ACCOUNT_TAKE_2': "Account_Take_2",
            'ACCOUNT_TAKE_3': "Account_Take_3",
            'ACCOUNT_TAKE_4': "Account_Take_4",
            'ACCOUNT_TAKE_5': "Account_Take_5",
            'ACCOUNT_TAKE_6': "Account_Take_6",
            'ACCOUNT_TAKE_7': "Account_Take_7",
            'ACCOUNTANT': "Accountant",
            'AUDITOR': "Auditor",
            'BRAND_MANAGER': "Brand_Manager",
            'COMMUNICATIONS_OFFICER': "Communications_Officer",
            'CONFIG_EQUIPMENT': "Config_Equipment",
            'CONFIG_STARBASE_EQUIPMENT': "Config_Starbase_Equipment",
            'CONTAINER_TAKE_1': "Container_Take_1",
            'CONTAINER_TAKE_2': "Container_Take_2",
            'CONTAINER_TAKE_3': "Container_Take_3",
            'CONTAINER_TAKE_4': "Container_Take_4",
            'CONTAINER_TAKE_5': "Container_Take_5",
            'CONTAINER_TAKE_6': "Container_Take_6",
            'CONTAINER_TAKE_7': "Container_Take_7",
            'CONTRACT_MANAGER': "Contract_Manager",
            'DELIVERIES_CONTAINER_TAKE': "Deliveries_Container_Take",
            'DELIVERIES_QUERY': "Deliveries_Query",
            'DELIVERIES_TAKE': "Deliveries_Take",
            'DIPLOMAT': "Diplomat",
            'DIRECTOR': "Director",
            'FACTORY_MANAGER': "Factory_Manager",
            'FITTING_MANAGER': "Fitting_Manager",
            'HANGAR_QUERY_1': "Hangar_Query_1",
            'HANGAR_QUERY_2': "Hangar_Query_2",
            'HANGAR_QUERY_3': "Hangar_Query_3",
            'HANGAR_QUERY_4': "Hangar_Query_4",
            'HANGAR_QUERY_5': "Hangar_Query_5",
            'HANGAR_QUERY_6': "Hangar_Query_6",
            'HANGAR_QUERY_7': "Hangar_Query_7",
            'HANGAR_TAKE_1': "Hangar_Take_1",
            'HANGAR_TAKE_2': "Hangar_Take_2",
            'HANGAR_TAKE_3': "Hangar_Take_3",
            'HANGAR_TAKE_4': "Hangar_Take_4",
            'HANGAR_TAKE_5': "Hangar_Take_5",
            'HANGAR_TAKE_6': "Hangar_Take_6",
            'HANGAR_TAKE_7': "Hangar_Take_7",
            'JUNIOR_ACCOUNTANT': "Junior_Accountant",
            'PERSONNEL_MANAGER': "Personnel_Manager",
            'PROJECT_MANAGER': "Project_Manager",
            'RENT_FACTORY_FACILITY': "Rent_Factory_Facility",
            'RENT_OFFICE': "Rent_Office",
            'RENT_RESEARCH_FACILITY': "Rent_Research_Facility",
            'SECURITY_OFFICER': "Security_Officer",
            'SKILL_PLAN_MANAGER': "Skill_Plan_Manager",
            'STARBASE_DEFENSE_OPERATOR': "Starbase_Defense_Operator",
            'STARBASE_FUEL_TECHNICIAN': "Starbase_Fuel_Technician",
            'STATION_MANAGER': "Station_Manager",
            'TRADER': "Trader",
        },
        ('roles_at_base',): {
            'ACCOUNT_TAKE_1': "Account_Take_1",
            'ACCOUNT_TAKE_2': "Account_Take_2",
            'ACCOUNT_TAKE_3': "Account_Take_3",
            'ACCOUNT_TAKE_4': "Account_Take_4",
            'ACCOUNT_TAKE_5': "Account_Take_5",
            'ACCOUNT_TAKE_6': "Account_Take_6",
            'ACCOUNT_TAKE_7': "Account_Take_7",
            'ACCOUNTANT': "Accountant",
            'AUDITOR': "Auditor",
            'BRAND_MANAGER': "Brand_Manager",
            'COMMUNICATIONS_OFFICER': "Communications_Officer",
            'CONFIG_EQUIPMENT': "Config_Equipment",
            'CONFIG_STARBASE_EQUIPMENT': "Config_Starbase_Equipment",
            'CONTAINER_TAKE_1': "Container_Take_1",
            'CONTAINER_TAKE_2': "Container_Take_2",
            'CONTAINER_TAKE_3': "Container_Take_3",
            'CONTAINER_TAKE_4': "Container_Take_4",
            'CONTAINER_TAKE_5': "Container_Take_5",
            'CONTAINER_TAKE_6': "Container_Take_6",
            'CONTAINER_TAKE_7': "Container_Take_7",
            'CONTRACT_MANAGER': "Contract_Manager",
            'DELIVERIES_CONTAINER_TAKE': "Deliveries_Container_Take",
            'DELIVERIES_QUERY': "Deliveries_Query",
            'DELIVERIES_TAKE': "Deliveries_Take",
            'DIPLOMAT': "Diplomat",
            'DIRECTOR': "Director",
            'FACTORY_MANAGER': "Factory_Manager",
            'FITTING_MANAGER': "Fitting_Manager",
            'HANGAR_QUERY_1': "Hangar_Query_1",
            'HANGAR_QUERY_2': "Hangar_Query_2",
            'HANGAR_QUERY_3': "Hangar_Query_3",
            'HANGAR_QUERY_4': "Hangar_Query_4",
            'HANGAR_QUERY_5': "Hangar_Query_5",
            'HANGAR_QUERY_6': "Hangar_Query_6",
            'HANGAR_QUERY_7': "Hangar_Query_7",
            'HANGAR_TAKE_1': "Hangar_Take_1",
            'HANGAR_TAKE_2': "Hangar_Take_2",
            'HANGAR_TAKE_3': "Hangar_Take_3",
            'HANGAR_TAKE_4': "Hangar_Take_4",
            'HANGAR_TAKE_5': "Hangar_Take_5",
            'HANGAR_TAKE_6': "Hangar_Take_6",
            'HANGAR_TAKE_7': "Hangar_Take_7",
            'JUNIOR_ACCOUNTANT': "Junior_Accountant",
            'PERSONNEL_MANAGER': "Personnel_Manager",
            'PROJECT_MANAGER': "Project_Manager",
            'RENT_FACTORY_FACILITY': "Rent_Factory_Facility",
            'RENT_OFFICE': "Rent_Office",
            'RENT_RESEARCH_FACILITY': "Rent_Research_Facility",
            'SECURITY_OFFICER': "Security_Officer",
            'SKILL_PLAN_MANAGER': "Skill_Plan_Manager",
            'STARBASE_DEFENSE_OPERATOR': "Starbase_Defense_Operator",
            'STARBASE_FUEL_TECHNICIAN': "Starbase_Fuel_Technician",
            'STATION_MANAGER': "Station_Manager",
            'TRADER': "Trader",
        },
        ('roles_at_hq',): {
            'ACCOUNT_TAKE_1': "Account_Take_1",
            'ACCOUNT_TAKE_2': "Account_Take_2",
            'ACCOUNT_TAKE_3': "Account_Take_3",
            'ACCOUNT_TAKE_4': "Account_Take_4",
            'ACCOUNT_TAKE_5': "Account_Take_5",
            'ACCOUNT_TAKE_6': "Account_Take_6",
            'ACCOUNT_TAKE_7': "Account_Take_7",
            'ACCOUNTANT': "Accountant",
            'AUDITOR': "Auditor",
            'BRAND_MANAGER': "Brand_Manager",
            'COMMUNICATIONS_OFFICER': "Communications_Officer",
            'CONFIG_EQUIPMENT': "Config_Equipment",
            'CONFIG_STARBASE_EQUIPMENT': "Config_Starbase_Equipment",
            'CONTAINER_TAKE_1': "Container_Take_1",
            'CONTAINER_TAKE_2': "Container_Take_2",
            'CONTAINER_TAKE_3': "Container_Take_3",
            'CONTAINER_TAKE_4': "Container_Take_4",
            'CONTAINER_TAKE_5': "Container_Take_5",
            'CONTAINER_TAKE_6': "Container_Take_6",
            'CONTAINER_TAKE_7': "Container_Take_7",
            'CONTRACT_MANAGER': "Contract_Manager",
            'DELIVERIES_CONTAINER_TAKE': "Deliveries_Container_Take",
            'DELIVERIES_QUERY': "Deliveries_Query",
            'DELIVERIES_TAKE': "Deliveries_Take",
            'DIPLOMAT': "Diplomat",
            'DIRECTOR': "Director",
            'FACTORY_MANAGER': "Factory_Manager",
            'FITTING_MANAGER': "Fitting_Manager",
            'HANGAR_QUERY_1': "Hangar_Query_1",
            'HANGAR_QUERY_2': "Hangar_Query_2",
            'HANGAR_QUERY_3': "Hangar_Query_3",
            'HANGAR_QUERY_4': "Hangar_Query_4",
            'HANGAR_QUERY_5': "Hangar_Query_5",
            'HANGAR_QUERY_6': "Hangar_Query_6",
            'HANGAR_QUERY_7': "Hangar_Query_7",
            'HANGAR_TAKE_1': "Hangar_Take_1",
            'HANGAR_TAKE_2': "Hangar_Take_2",
            'HANGAR_TAKE_3': "Hangar_Take_3",
            'HANGAR_TAKE_4': "Hangar_Take_4",
            'HANGAR_TAKE_5': "Hangar_Take_5",
            'HANGAR_TAKE_6': "Hangar_Take_6",
            'HANGAR_TAKE_7': "Hangar_Take_7",
            'JUNIOR_ACCOUNTANT': "Junior_Accountant",
            'PERSONNEL_MANAGER': "Personnel_Manager",
            'PROJECT_MANAGER': "Project_Manager",
            'RENT_FACTORY_FACILITY': "Rent_Factory_Facility",
            'RENT_OFFICE': "Rent_Office",
            'RENT_RESEARCH_FACILITY': "Rent_Research_Facility",
            'SECURITY_OFFICER': "Security_Officer",
            'SKILL_PLAN_MANAGER': "Skill_Plan_Manager",
            'STARBASE_DEFENSE_OPERATOR': "Starbase_Defense_Operator",
            'STARBASE_FUEL_TECHNICIAN': "Starbase_Fuel_Technician",
            'STATION_MANAGER': "Station_Manager",
            'TRADER': "Trader",
        },
        ('roles_at_other',): {
            'ACCOUNT_TAKE_1': "Account_Take_1",
            'ACCOUNT_TAKE_2': "Account_Take_2",
            'ACCOUNT_TAKE_3': "Account_Take_3",
            'ACCOUNT_TAKE_4': "Account_Take_4",
            'ACCOUNT_TAKE_5': "Account_Take_5",
            'ACCOUNT_TAKE_6': "Account_Take_6",
            'ACCOUNT_TAKE_7': "Account_Take_7",
            'ACCOUNTANT': "Accountant",
            'AUDITOR': "Auditor",
            'BRAND_MANAGER': "Brand_Manager",
            'COMMUNICATIONS_OFFICER': "Communications_Officer",
            'CONFIG_EQUIPMENT': "Config_Equipment",
            'CONFIG_STARBASE_EQUIPMENT': "Config_Starbase_Equipment",
            'CONTAINER_TAKE_1': "Container_Take_1",
            'CONTAINER_TAKE_2': "Container_Take_2",
            'CONTAINER_TAKE_3': "Container_Take_3",
            'CONTAINER_TAKE_4': "Container_Take_4",
            'CONTAINER_TAKE_5': "Container_Take_5",
            'CONTAINER_TAKE_6': "Container_Take_6",
            'CONTAINER_TAKE_7': "Container_Take_7",
            'CONTRACT_MANAGER': "Contract_Manager",
            'DELIVERIES_CONTAINER_TAKE': "Deliveries_Container_Take",
            'DELIVERIES_QUERY': "Deliveries_Query",
            'DELIVERIES_TAKE': "Deliveries_Take",
            'DIPLOMAT': "Diplomat",
            'DIRECTOR': "Director",
            'FACTORY_MANAGER': "Factory_Manager",
            'FITTING_MANAGER': "Fitting_Manager",
            'HANGAR_QUERY_1': "Hangar_Query_1",
            'HANGAR_QUERY_2': "Hangar_Query_2",
            'HANGAR_QUERY_3': "Hangar_Query_3",
            'HANGAR_QUERY_4': "Hangar_Query_4",
            'HANGAR_QUERY_5': "Hangar_Query_5",
            'HANGAR_QUERY_6': "Hangar_Query_6",
            'HANGAR_QUERY_7': "Hangar_Query_7",
            'HANGAR_TAKE_1': "Hangar_Take_1",
            'HANGAR_TAKE_2': "Hangar_Take_2",
            'HANGAR_TAKE_3': "Hangar_Take_3",
            'HANGAR_TAKE_4': "Hangar_Take_4",
            'HANGAR_TAKE_5': "Hangar_Take_5",
            'HANGAR_TAKE_6': "Hangar_Take_6",
            'HANGAR_TAKE_7': "Hangar_Take_7",
            'JUNIOR_ACCOUNTANT': "Junior_Accountant",
            'PERSONNEL_MANAGER': "Personnel_Manager",
            'PROJECT_MANAGER': "Project_Manager",
            'RENT_FACTORY_FACILITY': "Rent_Factory_Facility",
            'RENT_OFFICE': "Rent_Office",
            'RENT_RESEARCH_FACILITY': "Rent_Research_Facility",
            'SECURITY_OFFICER': "Security_Officer",
            'SKILL_PLAN_MANAGER': "Skill_Plan_Manager",
            'STARBASE_DEFENSE_OPERATOR': "Starbase_Defense_Operator",
            'STARBASE_FUEL_TECHNICIAN': "Starbase_Fuel_Technician",
            'STATION_MANAGER': "Station_Manager",
            'TRADER': "Trader",
        },
    }

    validations = {
        ('roles',): {
            'max_items': 100,
        },
        ('roles_at_base',): {
            'max_items': 100,
        },
        ('roles_at_hq',): {
            'max_items': 100,
        },
        ('roles_at_other',): {
            'max_items': 100,
        },
    }

    @cached_property
    def additional_properties_type():
        """
        This must be a method because a model may have properties that are
        of type self, this must run after the class is loaded
        """
        return (bool, date, datetime, dict, float, int, list, str, none_type,)  # noqa: E501

    _nullable = False

    @cached_property
    def openapi_types():
        """
        This must be a method because a model may have properties that are
        of type self, this must run after the class is loaded

        Returns
            openapi_types (dict): The key is attribute name
                and the value is attribute type.
        """
        return {
            'roles': ([str],),  # noqa: E501
            'roles_at_base': ([str],),  # noqa: E501
            'roles_at_hq': ([str],),  # noqa: E501
            'roles_at_other': ([str],),  # noqa: E501
        }

    @cached_property
    def discriminator():
        return None


    attribute_map = {
        'roles': 'roles',  # noqa: E501
        'roles_at_base': 'roles_at_base',  # noqa: E501
        'roles_at_hq': 'roles_at_hq',  # noqa: E501
        'roles_at_other': 'roles_at_other',  # noqa: E501
    }

    read_only_vars = {
    }

    _composed_schemas = {}

    @classmethod
    @convert_js_args_to_python_args
    def _from_openapi_data(cls, *args, **kwargs):  # noqa: E501
        """GetCharactersCharacterIdRolesOk - a model defined in OpenAPI

        Keyword Args:
            _check_type (bool): if True, values for parameters in openapi_types
                                will be type checked and a TypeError will be
                                raised if the wrong type is input.
                                Defaults to True
            _path_to_item (tuple/list): This is a list of keys or values to
                                drill down to the model in received_data
                                when deserializing a response
            _spec_property_naming (bool): True if the variable names in the input data
                                are serialized names, as specified in the OpenAPI document.
                                False if the variable names in the input data
                                are pythonic names, e.g. snake case (default)
            _configuration (Configuration): the instance to use when
                                deserializing a file_type parameter.
                                If passed, type conversion is attempted
                                If omitted no type conversion is done.
            _visited_composed_classes (tuple): This stores a tuple of
                                classes that we have traveled through so that
                                if we see that class again we will not use its
                                discriminator again.
                                When traveling through a discriminator, the
                                composed schema that is
                                is traveled through is added to this set.
                                For example if Animal has a discriminator
                                petType and we pass in "Dog", and the class Dog
                                allOf includes Animal, we move through Animal
                                once using the discriminator, and pick Dog.
                                Then in Dog, we will make an instance of the
                                Animal class but this time we won't travel
                                through its discriminator because we passed in
                                _visited_composed_classes = (Animal,)
            roles ([str]): roles array. [optional]  # noqa: E501
            roles_at_base ([str]): roles_at_base array. [optional]  # noqa: E501
            roles_at_hq ([str]): roles_at_hq array. [optional]  # noqa: E501
            roles_at_other ([str]): roles_at_other array. [optional]  # noqa: E501
        """

        _check_type = kwargs.pop('_check_type', True)
        _spec_property_naming = kwargs.pop('_spec_property_naming', True)
        _path_to_item = kwargs.pop('_path_to_item', ())
        _configuration = kwargs.pop('_configuration', None)
        _visited_composed_classes = kwargs.pop('_visited_composed_classes', ())

        self = super(OpenApiModel, cls).__new__(cls)

        if args:
            for arg in args:
                if isinstance(arg, dict):
                    kwargs.update(arg)
                else:
                    raise ApiTypeError(
                        "Invalid positional arguments=%s passed to %s. Remove those invalid positional arguments." % (
                            args,
                            self.__class__.__name__,
                        ),
                        path_to_item=_path_to_item,
                        valid_classes=(self.__class__,),
                    )

        self._data_store = {}
        self._check_type = _check_type
        self._spec_property_naming = _spec_property_naming
        self._path_to_item = _path_to_item
        self._configuration = _configuration
        self._visited_composed_classes = _visited_composed_classes + (self.__class__,)

        for var_name, var_value in kwargs.items():
            if var_name not in self.attribute_map and \
                        self._configuration is not None and \
                        self._configuration.discard_unknown_keys and \
                        self.additional_properties_type is None:
                # discard variable.
                continue
            setattr(self, var_name, var_value)
        return self

    required_properties = set([
        '_data_store',
        '_check_type',
        '_spec_property_naming',
        '_path_to_item',
        '_configuration',
        '_visited_composed_classes',
    ])

    @convert_js_args_to_python_args
    def __init__(self, *args, **kwargs):  # noqa: E501
        """GetCharactersCharacterIdRolesOk - a model defined in OpenAPI

        Keyword Args:
            _check_type (bool): if True, values for parameters in openapi_types
                                will be type checked and a TypeError will be
                                raised if the wrong type is input.
                                Defaults to True
            _path_to_item (tuple/list): This is a list of keys or values to
                                drill down to the model in received_data
                                when deserializing a response
            _spec_property_naming (bool): True if the variable names in the input data
                                are serialized names, as specified in the OpenAPI document.
                                False if the variable names in the input data
                                are pythonic names, e.g. snake case (default)
            _configuration (Configuration): the instance to use when
                                deserializing a file_type parameter.
                                If passed, type conversion is attempted
                                If omitted no type conversion is done.
            _visited_composed_classes (tuple): This stores a tuple of
                                classes that we have traveled through so that
                                if we see that class again we will not use its
                                discriminator again.
                                When traveling through a discriminator, the
                                composed schema that is
                                is traveled through is added to this set.
                                For example if Animal has a discriminator
                                petType and we pass in "Dog", and the class Dog
                                allOf includes Animal, we move through Animal
                                once using the discriminator, and pick Dog.
                                Then in Dog, we will make an instance of the
                                Animal class but this time we won't travel
                                through its discriminator because we passed in
                                _visited_composed_classes = (Animal,)
            roles ([str]): roles array. [optional]  # noqa: E501
            roles_at_base ([str]): roles_at_base array. [optional]  # noqa: E501
            roles_at_hq ([str]): roles_at_hq array. [optional]  # noqa: E501
            roles_at_other ([str]): roles_at_other array. [optional]  # noqa: E501
        """

        _check_type = kwargs.pop('_check_type', True)
        _spec_property_naming = kwargs.pop('_spec_property_naming', False)
        _path_to_item = kwargs.pop('_path_to_item', ())
        _configuration = kwargs.pop('_configuration', None)
        _visited_composed_classes = kwargs.pop('_visited_composed_classes', ())

        if args:
            for arg in args:
                if isinstance(arg, dict):
                    kwargs.update(arg)
                else:
                    raise ApiTypeError(
                        "Invalid positional arguments=%s passed to %s. Remove those invalid positional arguments." % (
                            args,
                            self.__class__.__name__,
                        ),
                        path_to_item=_path_to_item,
                        valid_classes=(self.__class__,),
                    )

        self._data_store = {}
        self._check_type = _check_type
        self._spec_property_naming = _spec_property_naming
        self._path_to_item = _path_to_item
        self._configuration = _configuration
        self._visited_composed_classes = _visited_composed_classes + (self.__class__,)

        for var_name, var_value in kwargs.items():
            if var_name not in self.attribute_map and \
                        self._configuration is not None and \
                        self._configuration.discard_unknown_keys and \
                        self.additional_properties_type is None:
                # discard variable.
                continue
            setattr(self, var_name, var_value)
            if var_name in self.read_only_vars:
                raise ApiAttributeError(f"`{var_name}` is a read-only attribute. Use `from_openapi_data` to instantiate "
                                     f"class with read only attributes.")
