"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from esi_client.model_utils import (  # noqa: F401
    ApiTypeError,
    ModelComposed,
    ModelNormal,
    ModelSimple,
    cached_property,
    change_keys_js_to_python,
    convert_js_args_to_python_args,
    date,
    datetime,
    file_type,
    none_type,
    validate_get_composed_info,
    OpenApiModel
)
from esi_client.exceptions import ApiAttributeError



class GetCharactersCharacterIdNotifications200Ok(ModelNormal):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.

    Attributes:
      allowed_values (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          with a capitalized key describing the allowed value and an allowed
          value. These dicts store the allowed enum values.
      attribute_map (dict): The key is attribute name
          and the value is json key in definition.
      discriminator_value_class_map (dict): A dict to go from the discriminator
          variable value to the discriminator class name.
      validations (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          that stores validations for max_length, min_length, max_items,
          min_items, exclusive_maximum, inclusive_maximum, exclusive_minimum,
          inclusive_minimum, and regex.
      additional_properties_type (tuple): A tuple of classes accepted
          as additional properties values.
    """

    allowed_values = {
        ('sender_type',): {
            'CHARACTER': "character",
            'CORPORATION': "corporation",
            'ALLIANCE': "alliance",
            'FACTION': "faction",
            'OTHER': "other",
        },
        ('type',): {
            'ACCEPTEDALLY': "AcceptedAlly",
            'ACCEPTEDSURRENDER': "AcceptedSurrender",
            'AGENTRETIREDTRIGRAVIAN': "AgentRetiredTrigravian",
            'ALLANCHORINGMSG': "AllAnchoringMsg",
            'ALLMAINTENANCEBILLMSG': "AllMaintenanceBillMsg",
            'ALLSTRUCINVULNERABLEMSG': "AllStrucInvulnerableMsg",
            'ALLSTRUCTVULNERABLEMSG': "AllStructVulnerableMsg",
            'ALLWARCORPJOINEDALLIANCEMSG': "AllWarCorpJoinedAllianceMsg",
            'ALLWARDECLAREDMSG': "AllWarDeclaredMsg",
            'ALLWARINVALIDATEDMSG': "AllWarInvalidatedMsg",
            'ALLWARRETRACTEDMSG': "AllWarRetractedMsg",
            'ALLWARSURRENDERMSG': "AllWarSurrenderMsg",
            'ALLIANCECAPITALCHANGED': "AllianceCapitalChanged",
            'ALLIANCEWARDECLAREDV2': "AllianceWarDeclaredV2",
            'ALLYCONTRACTCANCELLED': "AllyContractCancelled",
            'ALLYJOINEDWARAGGRESSORMSG': "AllyJoinedWarAggressorMsg",
            'ALLYJOINEDWARALLYMSG': "AllyJoinedWarAllyMsg",
            'ALLYJOINEDWARDEFENDERMSG': "AllyJoinedWarDefenderMsg",
            'BATTLEPUNISHFRIENDLYFIRE': "BattlePunishFriendlyFire",
            'BILLOUTOFMONEYMSG': "BillOutOfMoneyMsg",
            'BILLPAIDCORPALLMSG': "BillPaidCorpAllMsg",
            'BOUNTYCLAIMMSG': "BountyClaimMsg",
            'BOUNTYESSSHARED': "BountyESSShared",
            'BOUNTYESSTAKEN': "BountyESSTaken",
            'BOUNTYPLACEDALLIANCE': "BountyPlacedAlliance",
            'BOUNTYPLACEDCHAR': "BountyPlacedChar",
            'BOUNTYPLACEDCORP': "BountyPlacedCorp",
            'BOUNTYYOURBOUNTYCLAIMED': "BountyYourBountyClaimed",
            'BUDDYCONNECTCONTACTADD': "BuddyConnectContactAdd",
            'CHARAPPACCEPTMSG': "CharAppAcceptMsg",
            'CHARAPPREJECTMSG': "CharAppRejectMsg",
            'CHARAPPWITHDRAWMSG': "CharAppWithdrawMsg",
            'CHARLEFTCORPMSG': "CharLeftCorpMsg",
            'CHARMEDALMSG': "CharMedalMsg",
            'CHARTERMINATIONMSG': "CharTerminationMsg",
            'CLONEACTIVATIONMSG': "CloneActivationMsg",
            'CLONEACTIVATIONMSG2': "CloneActivationMsg2",
            'CLONEMOVEDMSG': "CloneMovedMsg",
            'CLONEREVOKEDMSG1': "CloneRevokedMsg1",
            'CLONEREVOKEDMSG2': "CloneRevokedMsg2",
            'COMBATOPERATIONFINISHED': "CombatOperationFinished",
            'CONTACTADD': "ContactAdd",
            'CONTACTEDIT': "ContactEdit",
            'CONTAINERPASSWORDMSG': "ContainerPasswordMsg",
            'CONTRACTREGIONCHANGEDTOPOCHVEN': "ContractRegionChangedToPochven",
            'CORPALLBILLMSG': "CorpAllBillMsg",
            'CORPAPPACCEPTMSG': "CorpAppAcceptMsg",
            'CORPAPPINVITEDMSG': "CorpAppInvitedMsg",
            'CORPAPPNEWMSG': "CorpAppNewMsg",
            'CORPAPPREJECTCUSTOMMSG': "CorpAppRejectCustomMsg",
            'CORPAPPREJECTMSG': "CorpAppRejectMsg",
            'CORPBECAMEWARELIGIBLE': "CorpBecameWarEligible",
            'CORPDIVIDENDMSG': "CorpDividendMsg",
            'CORPFRIENDLYFIREDISABLETIMERCOMPLETED': "CorpFriendlyFireDisableTimerCompleted",
            'CORPFRIENDLYFIREDISABLETIMERSTARTED': "CorpFriendlyFireDisableTimerStarted",
            'CORPFRIENDLYFIREENABLETIMERCOMPLETED': "CorpFriendlyFireEnableTimerCompleted",
            'CORPFRIENDLYFIREENABLETIMERSTARTED': "CorpFriendlyFireEnableTimerStarted",
            'CORPKICKED': "CorpKicked",
            'CORPLIQUIDATIONMSG': "CorpLiquidationMsg",
            'CORPNEWCEOMSG': "CorpNewCEOMsg",
            'CORPNEWSMSG': "CorpNewsMsg",
            'CORPNOLONGERWARELIGIBLE': "CorpNoLongerWarEligible",
            'CORPOFFICEEXPIRATIONMSG': "CorpOfficeExpirationMsg",
            'CORPSTRUCTLOSTMSG': "CorpStructLostMsg",
            'CORPTAXCHANGEMSG': "CorpTaxChangeMsg",
            'CORPVOTECEOREVOKEDMSG': "CorpVoteCEORevokedMsg",
            'CORPVOTEMSG': "CorpVoteMsg",
            'CORPWARDECLAREDMSG': "CorpWarDeclaredMsg",
            'CORPWARDECLAREDV2': "CorpWarDeclaredV2",
            'CORPWARFIGHTINGLEGALMSG': "CorpWarFightingLegalMsg",
            'CORPWARINVALIDATEDMSG': "CorpWarInvalidatedMsg",
            'CORPWARRETRACTEDMSG': "CorpWarRetractedMsg",
            'CORPWARSURRENDERMSG': "CorpWarSurrenderMsg",
            'CORPORATIONGOALCLOSED': "CorporationGoalClosed",
            'CORPORATIONGOALCOMPLETED': "CorporationGoalCompleted",
            'CORPORATIONGOALCREATED': "CorporationGoalCreated",
            'CORPORATIONGOALEXPIRED': "CorporationGoalExpired",
            'CORPORATIONGOALLIMITREACHED': "CorporationGoalLimitReached",
            'CORPORATIONGOALNAMECHANGE': "CorporationGoalNameChange",
            'CORPORATIONLEFT': "CorporationLeft",
            'CUSTOMSMSG': "CustomsMsg",
            'DAILYITEMREWARDAUTOCLAIMED': "DailyItemRewardAutoClaimed",
            'DECLAREWAR': "DeclareWar",
            'DISTRICTATTACKED': "DistrictAttacked",
            'DUSTAPPACCEPTEDMSG': "DustAppAcceptedMsg",
            'ESSMAINBANKLINK': "ESSMainBankLink",
            'ENTOSISCAPTURESTARTED': "EntosisCaptureStarted",
            'EXPERTSYSTEMEXPIRED': "ExpertSystemExpired",
            'EXPERTSYSTEMEXPIRYIMMINENT': "ExpertSystemExpiryImminent",
            'FWALLIANCEKICKMSG': "FWAllianceKickMsg",
            'FWALLIANCEWARNINGMSG': "FWAllianceWarningMsg",
            'FWCHARKICKMSG': "FWCharKickMsg",
            'FWCHARRANKGAINMSG': "FWCharRankGainMsg",
            'FWCHARRANKLOSSMSG': "FWCharRankLossMsg",
            'FWCHARWARNINGMSG': "FWCharWarningMsg",
            'FWCORPJOINMSG': "FWCorpJoinMsg",
            'FWCORPKICKMSG': "FWCorpKickMsg",
            'FWCORPLEAVEMSG': "FWCorpLeaveMsg",
            'FWCORPWARNINGMSG': "FWCorpWarningMsg",
            'FACWARCORPJOINREQUESTMSG': "FacWarCorpJoinRequestMsg",
            'FACWARCORPJOINWITHDRAWMSG': "FacWarCorpJoinWithdrawMsg",
            'FACWARCORPLEAVEREQUESTMSG': "FacWarCorpLeaveRequestMsg",
            'FACWARCORPLEAVEWITHDRAWMSG': "FacWarCorpLeaveWithdrawMsg",
            'FACWARLPDISQUALIFIEDEVENT': "FacWarLPDisqualifiedEvent",
            'FACWARLPDISQUALIFIEDKILL': "FacWarLPDisqualifiedKill",
            'FACWARLPPAYOUTEVENT': "FacWarLPPayoutEvent",
            'FACWARLPPAYOUTKILL': "FacWarLPPayoutKill",
            'GAMETIMEADDED': "GameTimeAdded",
            'GAMETIMERECEIVED': "GameTimeReceived",
            'GAMETIMESENT': "GameTimeSent",
            'GIFTRECEIVED': "GiftReceived",
            'IHUBDESTROYEDBYBILLFAILURE': "IHubDestroyedByBillFailure",
            'INCURSIONCOMPLETEDMSG': "IncursionCompletedMsg",
            'INDUSTRYOPERATIONFINISHED': "IndustryOperationFinished",
            'INDUSTRYTEAMAUCTIONLOST': "IndustryTeamAuctionLost",
            'INDUSTRYTEAMAUCTIONWON': "IndustryTeamAuctionWon",
            'INFRASTRUCTUREHUBBILLABOUTTOEXPIRE': "InfrastructureHubBillAboutToExpire",
            'INSURANCEEXPIRATIONMSG': "InsuranceExpirationMsg",
            'INSURANCEFIRSTSHIPMSG': "InsuranceFirstShipMsg",
            'INSURANCEINVALIDATEDMSG': "InsuranceInvalidatedMsg",
            'INSURANCEISSUEDMSG': "InsuranceIssuedMsg",
            'INSURANCEPAYOUTMSG': "InsurancePayoutMsg",
            'INVASIONCOMPLETEDMSG': "InvasionCompletedMsg",
            'INVASIONSYSTEMLOGIN': "InvasionSystemLogin",
            'INVASIONSYSTEMSTART': "InvasionSystemStart",
            'JUMPCLONEDELETEDMSG1': "JumpCloneDeletedMsg1",
            'JUMPCLONEDELETEDMSG2': "JumpCloneDeletedMsg2",
            'KILLREPORTFINALBLOW': "KillReportFinalBlow",
            'KILLREPORTVICTIM': "KillReportVictim",
            'KILLRIGHTAVAILABLE': "KillRightAvailable",
            'KILLRIGHTAVAILABLEOPEN': "KillRightAvailableOpen",
            'KILLRIGHTEARNED': "KillRightEarned",
            'KILLRIGHTUNAVAILABLE': "KillRightUnavailable",
            'KILLRIGHTUNAVAILABLEOPEN': "KillRightUnavailableOpen",
            'KILLRIGHTUSED': "KillRightUsed",
            'LPAUTOREDEEMED': "LPAutoRedeemed",
            'LOCATECHARMSG': "LocateCharMsg",
            'MADEWARMUTUAL': "MadeWarMutual",
            'MERCOFFERRETRACTEDMSG': "MercOfferRetractedMsg",
            'MERCOFFEREDNEGOTIATIONMSG': "MercOfferedNegotiationMsg",
            'MERCENARYDENATTACKED': "MercenaryDenAttacked",
            'MERCENARYDENREINFORCED': "MercenaryDenReinforced",
            'MISSIONCANCELEDTRIGLAVIAN': "MissionCanceledTriglavian",
            'MISSIONOFFEREXPIRATIONMSG': "MissionOfferExpirationMsg",
            'MISSIONTIMEOUTMSG': "MissionTimeoutMsg",
            'MOONMININGAUTOMATICFRACTURE': "MoonminingAutomaticFracture",
            'MOONMININGEXTRACTIONCANCELLED': "MoonminingExtractionCancelled",
            'MOONMININGEXTRACTIONFINISHED': "MoonminingExtractionFinished",
            'MOONMININGEXTRACTIONSTARTED': "MoonminingExtractionStarted",
            'MOONMININGLASERFIRED': "MoonminingLaserFired",
            'MUTUALWAREXPIRED': "MutualWarExpired",
            'MUTUALWARINVITEACCEPTED': "MutualWarInviteAccepted",
            'MUTUALWARINVITEREJECTED': "MutualWarInviteRejected",
            'MUTUALWARINVITESENT': "MutualWarInviteSent",
            'NPCSTANDINGSGAINED': "NPCStandingsGained",
            'NPCSTANDINGSLOST': "NPCStandingsLost",
            'OFFERTOALLYRETRACTED': "OfferToAllyRetracted",
            'OFFEREDSURRENDER': "OfferedSurrender",
            'OFFEREDTOALLY': "OfferedToAlly",
            'OFFICELEASECANCELEDINSUFFICIENTSTANDINGS': "OfficeLeaseCanceledInsufficientStandings",
            'OLDLSCMESSAGES': "OldLscMessages",
            'OPERATIONFINISHED': "OperationFinished",
            'ORBITALATTACKED': "OrbitalAttacked",
            'ORBITALREINFORCED': "OrbitalReinforced",
            'OWNERSHIPTRANSFERRED': "OwnershipTransferred",
            'RAFFLECREATED': "RaffleCreated",
            'RAFFLEEXPIRED': "RaffleExpired",
            'RAFFLEFINISHED': "RaffleFinished",
            'REIMBURSEMENTMSG': "ReimbursementMsg",
            'RESEARCHMISSIONAVAILABLEMSG': "ResearchMissionAvailableMsg",
            'RETRACTSWAR': "RetractsWar",
            'SPAUTOREDEEMED': "SPAutoRedeemed",
            'SEASONALCHALLENGECOMPLETED': "SeasonalChallengeCompleted",
            'SKINSEQUENCINGCOMPLETED': "SkinSequencingCompleted",
            'SKYHOOKDEPLOYED': "SkyhookDeployed",
            'SKYHOOKDESTROYED': "SkyhookDestroyed",
            'SKYHOOKLOSTSHIELDS': "SkyhookLostShields",
            'SKYHOOKONLINE': "SkyhookOnline",
            'SKYHOOKUNDERATTACK': "SkyhookUnderAttack",
            'SOVALLCLAIMAQUIREDMSG': "SovAllClaimAquiredMsg",
            'SOVALLCLAIMLOSTMSG': "SovAllClaimLostMsg",
            'SOVCOMMANDNODEEVENTSTARTED': "SovCommandNodeEventStarted",
            'SOVCORPBILLLATEMSG': "SovCorpBillLateMsg",
            'SOVCORPCLAIMFAILMSG': "SovCorpClaimFailMsg",
            'SOVDISRUPTORMSG': "SovDisruptorMsg",
            'SOVSTATIONENTEREDFREEPORT': "SovStationEnteredFreeport",
            'SOVSTRUCTUREDESTROYED': "SovStructureDestroyed",
            'SOVSTRUCTUREREINFORCED': "SovStructureReinforced",
            'SOVSTRUCTURESELFDESTRUCTCANCEL': "SovStructureSelfDestructCancel",
            'SOVSTRUCTURESELFDESTRUCTFINISHED': "SovStructureSelfDestructFinished",
            'SOVSTRUCTURESELFDESTRUCTREQUESTED': "SovStructureSelfDestructRequested",
            'SOVEREIGNTYIHDAMAGEMSG': "SovereigntyIHDamageMsg",
            'SOVEREIGNTYSBUDAMAGEMSG': "SovereigntySBUDamageMsg",
            'SOVEREIGNTYTCUDAMAGEMSG': "SovereigntyTCUDamageMsg",
            'STATIONAGGRESSIONMSG1': "StationAggressionMsg1",
            'STATIONAGGRESSIONMSG2': "StationAggressionMsg2",
            'STATIONCONQUERMSG': "StationConquerMsg",
            'STATIONSERVICEDISABLED': "StationServiceDisabled",
            'STATIONSERVICEENABLED': "StationServiceEnabled",
            'STATIONSTATECHANGEMSG': "StationStateChangeMsg",
            'STORYLINEMISSIONAVAILABLEMSG': "StoryLineMissionAvailableMsg",
            'STRUCTUREANCHORING': "StructureAnchoring",
            'STRUCTURECOURIERCONTRACTCHANGED': "StructureCourierContractChanged",
            'STRUCTUREDESTROYED': "StructureDestroyed",
            'STRUCTUREFUELALERT': "StructureFuelAlert",
            'STRUCTUREIMPENDINGABANDONMENTASSETSATRISK': "StructureImpendingAbandonmentAssetsAtRisk",
            'STRUCTUREITEMSDELIVERED': "StructureItemsDelivered",
            'STRUCTUREITEMSMOVEDTOSAFETY': "StructureItemsMovedToSafety",
            'STRUCTURELOSTARMOR': "StructureLostArmor",
            'STRUCTURELOSTSHIELDS': "StructureLostShields",
            'STRUCTURELOWREAGENTSALERT': "StructureLowReagentsAlert",
            'STRUCTURENOREAGENTSALERT': "StructureNoReagentsAlert",
            'STRUCTUREONLINE': "StructureOnline",
            'STRUCTUREPAINTPURCHASED': "StructurePaintPurchased",
            'STRUCTURESERVICESOFFLINE': "StructureServicesOffline",
            'STRUCTUREUNANCHORING': "StructureUnanchoring",
            'STRUCTUREUNDERATTACK': "StructureUnderAttack",
            'STRUCTUREWENTHIGHPOWER': "StructureWentHighPower",
            'STRUCTUREWENTLOWPOWER': "StructureWentLowPower",
            'STRUCTURESJOBSCANCELLED': "StructuresJobsCancelled",
            'STRUCTURESJOBSPAUSED': "StructuresJobsPaused",
            'STRUCTURESREINFORCEMENTCHANGED': "StructuresReinforcementChanged",
            'TOWERALERTMSG': "TowerAlertMsg",
            'TOWERRESOURCEALERTMSG': "TowerResourceAlertMsg",
            'TRANSACTIONREVERSALMSG': "TransactionReversalMsg",
            'TUTORIALMSG': "TutorialMsg",
            'WARADOPTED_': "WarAdopted ",
            'WARALLYINHERITED': "WarAllyInherited",
            'WARALLYOFFERDECLINEDMSG': "WarAllyOfferDeclinedMsg",
            'WARCONCORDINVALIDATES': "WarConcordInvalidates",
            'WARDECLARED': "WarDeclared",
            'WARENDEDHQSECURITYDROP': "WarEndedHqSecurityDrop",
            'WARHQREMOVEDFROMSPACE': "WarHQRemovedFromSpace",
            'WARINHERITED': "WarInherited",
            'WARINVALID': "WarInvalid",
            'WARRETRACTED': "WarRetracted",
            'WARRETRACTEDBYCONCORD': "WarRetractedByConcord",
            'WARSURRENDERDECLINEDMSG': "WarSurrenderDeclinedMsg",
            'WARSURRENDEROFFERMSG': "WarSurrenderOfferMsg",
        },
    }

    validations = {
    }

    @cached_property
    def additional_properties_type():
        """
        This must be a method because a model may have properties that are
        of type self, this must run after the class is loaded
        """
        return (bool, date, datetime, dict, float, int, list, str, none_type,)  # noqa: E501

    _nullable = False

    @cached_property
    def openapi_types():
        """
        This must be a method because a model may have properties that are
        of type self, this must run after the class is loaded

        Returns
            openapi_types (dict): The key is attribute name
                and the value is attribute type.
        """
        return {
            'notification_id': (int,),  # noqa: E501
            'sender_id': (int,),  # noqa: E501
            'sender_type': (str,),  # noqa: E501
            'timestamp': (datetime,),  # noqa: E501
            'type': (str,),  # noqa: E501
            'is_read': (bool,),  # noqa: E501
            'text': (str,),  # noqa: E501
        }

    @cached_property
    def discriminator():
        return None


    attribute_map = {
        'notification_id': 'notification_id',  # noqa: E501
        'sender_id': 'sender_id',  # noqa: E501
        'sender_type': 'sender_type',  # noqa: E501
        'timestamp': 'timestamp',  # noqa: E501
        'type': 'type',  # noqa: E501
        'is_read': 'is_read',  # noqa: E501
        'text': 'text',  # noqa: E501
    }

    read_only_vars = {
    }

    _composed_schemas = {}

    @classmethod
    @convert_js_args_to_python_args
    def _from_openapi_data(cls, notification_id, sender_id, sender_type, timestamp, type, *args, **kwargs):  # noqa: E501
        """GetCharactersCharacterIdNotifications200Ok - a model defined in OpenAPI

        Args:
            notification_id (int): notification_id integer
            sender_id (int): sender_id integer
            sender_type (str): sender_type string
            timestamp (datetime): timestamp string
            type (str): type string

        Keyword Args:
            _check_type (bool): if True, values for parameters in openapi_types
                                will be type checked and a TypeError will be
                                raised if the wrong type is input.
                                Defaults to True
            _path_to_item (tuple/list): This is a list of keys or values to
                                drill down to the model in received_data
                                when deserializing a response
            _spec_property_naming (bool): True if the variable names in the input data
                                are serialized names, as specified in the OpenAPI document.
                                False if the variable names in the input data
                                are pythonic names, e.g. snake case (default)
            _configuration (Configuration): the instance to use when
                                deserializing a file_type parameter.
                                If passed, type conversion is attempted
                                If omitted no type conversion is done.
            _visited_composed_classes (tuple): This stores a tuple of
                                classes that we have traveled through so that
                                if we see that class again we will not use its
                                discriminator again.
                                When traveling through a discriminator, the
                                composed schema that is
                                is traveled through is added to this set.
                                For example if Animal has a discriminator
                                petType and we pass in "Dog", and the class Dog
                                allOf includes Animal, we move through Animal
                                once using the discriminator, and pick Dog.
                                Then in Dog, we will make an instance of the
                                Animal class but this time we won't travel
                                through its discriminator because we passed in
                                _visited_composed_classes = (Animal,)
            is_read (bool): is_read boolean. [optional]  # noqa: E501
            text (str): text string. [optional]  # noqa: E501
        """

        _check_type = kwargs.pop('_check_type', True)
        _spec_property_naming = kwargs.pop('_spec_property_naming', True)
        _path_to_item = kwargs.pop('_path_to_item', ())
        _configuration = kwargs.pop('_configuration', None)
        _visited_composed_classes = kwargs.pop('_visited_composed_classes', ())

        self = super(OpenApiModel, cls).__new__(cls)

        if args:
            for arg in args:
                if isinstance(arg, dict):
                    kwargs.update(arg)
                else:
                    raise ApiTypeError(
                        "Invalid positional arguments=%s passed to %s. Remove those invalid positional arguments." % (
                            args,
                            self.__class__.__name__,
                        ),
                        path_to_item=_path_to_item,
                        valid_classes=(self.__class__,),
                    )

        self._data_store = {}
        self._check_type = _check_type
        self._spec_property_naming = _spec_property_naming
        self._path_to_item = _path_to_item
        self._configuration = _configuration
        self._visited_composed_classes = _visited_composed_classes + (self.__class__,)

        self.notification_id = notification_id
        self.sender_id = sender_id
        self.sender_type = sender_type
        self.timestamp = timestamp
        self.type = type
        for var_name, var_value in kwargs.items():
            if var_name not in self.attribute_map and \
                        self._configuration is not None and \
                        self._configuration.discard_unknown_keys and \
                        self.additional_properties_type is None:
                # discard variable.
                continue
            setattr(self, var_name, var_value)
        return self

    required_properties = set([
        '_data_store',
        '_check_type',
        '_spec_property_naming',
        '_path_to_item',
        '_configuration',
        '_visited_composed_classes',
    ])

    @convert_js_args_to_python_args
    def __init__(self, notification_id, sender_id, sender_type, timestamp, type, *args, **kwargs):  # noqa: E501
        """GetCharactersCharacterIdNotifications200Ok - a model defined in OpenAPI

        Args:
            notification_id (int): notification_id integer
            sender_id (int): sender_id integer
            sender_type (str): sender_type string
            timestamp (datetime): timestamp string
            type (str): type string

        Keyword Args:
            _check_type (bool): if True, values for parameters in openapi_types
                                will be type checked and a TypeError will be
                                raised if the wrong type is input.
                                Defaults to True
            _path_to_item (tuple/list): This is a list of keys or values to
                                drill down to the model in received_data
                                when deserializing a response
            _spec_property_naming (bool): True if the variable names in the input data
                                are serialized names, as specified in the OpenAPI document.
                                False if the variable names in the input data
                                are pythonic names, e.g. snake case (default)
            _configuration (Configuration): the instance to use when
                                deserializing a file_type parameter.
                                If passed, type conversion is attempted
                                If omitted no type conversion is done.
            _visited_composed_classes (tuple): This stores a tuple of
                                classes that we have traveled through so that
                                if we see that class again we will not use its
                                discriminator again.
                                When traveling through a discriminator, the
                                composed schema that is
                                is traveled through is added to this set.
                                For example if Animal has a discriminator
                                petType and we pass in "Dog", and the class Dog
                                allOf includes Animal, we move through Animal
                                once using the discriminator, and pick Dog.
                                Then in Dog, we will make an instance of the
                                Animal class but this time we won't travel
                                through its discriminator because we passed in
                                _visited_composed_classes = (Animal,)
            is_read (bool): is_read boolean. [optional]  # noqa: E501
            text (str): text string. [optional]  # noqa: E501
        """

        _check_type = kwargs.pop('_check_type', True)
        _spec_property_naming = kwargs.pop('_spec_property_naming', False)
        _path_to_item = kwargs.pop('_path_to_item', ())
        _configuration = kwargs.pop('_configuration', None)
        _visited_composed_classes = kwargs.pop('_visited_composed_classes', ())

        if args:
            for arg in args:
                if isinstance(arg, dict):
                    kwargs.update(arg)
                else:
                    raise ApiTypeError(
                        "Invalid positional arguments=%s passed to %s. Remove those invalid positional arguments." % (
                            args,
                            self.__class__.__name__,
                        ),
                        path_to_item=_path_to_item,
                        valid_classes=(self.__class__,),
                    )

        self._data_store = {}
        self._check_type = _check_type
        self._spec_property_naming = _spec_property_naming
        self._path_to_item = _path_to_item
        self._configuration = _configuration
        self._visited_composed_classes = _visited_composed_classes + (self.__class__,)

        self.notification_id = notification_id
        self.sender_id = sender_id
        self.sender_type = sender_type
        self.timestamp = timestamp
        self.type = type
        for var_name, var_value in kwargs.items():
            if var_name not in self.attribute_map and \
                        self._configuration is not None and \
                        self._configuration.discard_unknown_keys and \
                        self.additional_properties_type is None:
                # discard variable.
                continue
            setattr(self, var_name, var_value)
            if var_name in self.read_only_vars:
                raise ApiAttributeError(f"`{var_name}` is a read-only attribute. Use `from_openapi_data` to instantiate "
                                     f"class with read only attributes.")
