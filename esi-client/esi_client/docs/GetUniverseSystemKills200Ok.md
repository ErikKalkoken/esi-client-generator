# GetUniverseSystemKills200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**npc_kills** | **int** | Number of NPC ships killed in this system | 
**pod_kills** | **int** | Number of pods killed in this system | 
**ship_kills** | **int** | Number of player ships killed in this system | 
**system_id** | **int** | system_id integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


