# GetCharactersCharacterIdMailLabelsLabel

label object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **str** | color string | [optional]  if omitted the server will use the default value of "#ffffff"
**label_id** | **int** | label_id integer | [optional] 
**name** | **str** | name string | [optional] 
**unread_count** | **int** | unread_count integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


