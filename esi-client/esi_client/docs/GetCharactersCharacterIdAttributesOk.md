# GetCharactersCharacterIdAttributesOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**charisma** | **int** | charisma integer | 
**intelligence** | **int** | intelligence integer | 
**memory** | **int** | memory integer | 
**perception** | **int** | perception integer | 
**willpower** | **int** | willpower integer | 
**accrued_remap_cooldown_date** | **datetime** | Neural remapping cooldown after a character uses remap accrued over time | [optional] 
**bonus_remaps** | **int** | Number of available bonus character neural remaps | [optional] 
**last_remap_date** | **datetime** | Datetime of last neural remap, including usage of bonus remaps | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


