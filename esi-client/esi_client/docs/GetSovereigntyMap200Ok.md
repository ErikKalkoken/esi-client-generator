# GetSovereigntyMap200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**system_id** | **int** | system_id integer | 
**alliance_id** | **int** | alliance_id integer | [optional] 
**corporation_id** | **int** | corporation_id integer | [optional] 
**faction_id** | **int** | faction_id integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


