# GetCharactersCharacterIdFleetOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fleet_id** | **int** | The character&#39;s current fleet ID | 
**role** | **str** | Member’s role in fleet | 
**squad_id** | **int** | ID of the squad the member is in. If not applicable, will be set to -1 | 
**wing_id** | **int** | ID of the wing the member is in. If not applicable, will be set to -1 | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


