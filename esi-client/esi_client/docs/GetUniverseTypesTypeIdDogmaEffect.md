# GetUniverseTypesTypeIdDogmaEffect

dogma_effect object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**effect_id** | **int** | effect_id integer | 
**is_default** | **bool** | is_default boolean | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


