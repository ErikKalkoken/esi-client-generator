# GetUniverseGraphicsGraphicIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**graphic_id** | **int** | graphic_id integer | 
**collision_file** | **str** | collision_file string | [optional] 
**graphic_file** | **str** | graphic_file string | [optional] 
**icon_folder** | **str** | icon_folder string | [optional] 
**sof_dna** | **str** | sof_dna string | [optional] 
**sof_fation_name** | **str** | sof_fation_name string | [optional] 
**sof_hull_name** | **str** | sof_hull_name string | [optional] 
**sof_race_name** | **str** | sof_race_name string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


