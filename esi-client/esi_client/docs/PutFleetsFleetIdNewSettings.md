# PutFleetsFleetIdNewSettings

new_settings object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_free_move** | **bool** | Should free-move be enabled in the fleet | [optional] 
**motd** | **str** | New fleet MOTD in CCP flavoured HTML | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


