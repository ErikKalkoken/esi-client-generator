# GetStatusOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**players** | **int** | Current online player count | 
**server_version** | **str** | Running version as string | 
**start_time** | **datetime** | Server start timestamp | 
**vip** | **bool** | If the server is in VIP mode | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


