# GetAlliancesAllianceIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creator_corporation_id** | **int** | ID of the corporation that created the alliance | 
**creator_id** | **int** | ID of the character that created the alliance | 
**date_founded** | **datetime** | date_founded string | 
**name** | **str** | the full name of the alliance | 
**ticker** | **str** | the short name of the alliance | 
**executor_corporation_id** | **int** | the executor corporation ID, if this alliance is not closed | [optional] 
**faction_id** | **int** | Faction ID this alliance is fighting for, if this alliance is enlisted in factional warfare | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


