# GetCorporationsCorporationIdMedals200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **datetime** | created_at string | 
**creator_id** | **int** | ID of the character who created this medal | 
**description** | **str** | description string | 
**medal_id** | **int** | medal_id integer | 
**title** | **str** | title string | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


