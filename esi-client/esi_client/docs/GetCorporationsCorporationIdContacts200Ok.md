# GetCorporationsCorporationIdContacts200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact_id** | **int** | contact_id integer | 
**contact_type** | **str** | contact_type string | 
**standing** | **float** | Standing of the contact | 
**is_watched** | **bool** | Whether this contact is being watched | [optional] 
**label_ids** | **[int]** | label_ids array | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


