# GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails

extractor_details object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**heads** | [**[GetCharactersCharacterIdPlanetsPlanetIdHead]**](GetCharactersCharacterIdPlanetsPlanetIdHead.md) | heads array | 
**cycle_time** | **int** | in seconds | [optional] 
**head_radius** | **float** | head_radius number | [optional] 
**product_type_id** | **int** | product_type_id integer | [optional] 
**qty_per_cycle** | **int** | qty_per_cycle integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


