# GetLoyaltyStoresCorporationIdOffers200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isk_cost** | **int** | isk_cost integer | 
**lp_cost** | **int** | lp_cost integer | 
**offer_id** | **int** | offer_id integer | 
**quantity** | **int** | quantity integer | 
**required_items** | [**[GetLoyaltyStoresCorporationIdOffersRequiredItem]**](GetLoyaltyStoresCorporationIdOffersRequiredItem.md) | required_items array | 
**type_id** | **int** | type_id integer | 
**ak_cost** | **int** | Analysis kredit cost | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


