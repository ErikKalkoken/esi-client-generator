# GetMarketsRegionIdHistory200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**average** | **float** | average number | 
**date** | **date** | The date of this historical statistic entry | 
**highest** | **float** | highest number | 
**lowest** | **float** | lowest number | 
**order_count** | **int** | Total number of orders happened that day | 
**volume** | **int** | Total | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


