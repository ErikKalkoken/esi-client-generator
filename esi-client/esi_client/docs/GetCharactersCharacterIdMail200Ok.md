# GetCharactersCharacterIdMail200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_from** | **int** | From whom the mail was sent | [optional] 
**is_read** | **bool** | is_read boolean | [optional] 
**labels** | **[int]** | labels array | [optional] 
**mail_id** | **int** | mail_id integer | [optional] 
**recipients** | [**[GetCharactersCharacterIdMailRecipient]**](GetCharactersCharacterIdMailRecipient.md) | Recipients of the mail | [optional] 
**subject** | **str** | Mail subject | [optional] 
**timestamp** | **datetime** | When the mail was sent | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


