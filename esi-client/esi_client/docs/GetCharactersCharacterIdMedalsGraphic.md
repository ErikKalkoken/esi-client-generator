# GetCharactersCharacterIdMedalsGraphic

graphic object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**graphic** | **str** | graphic string | 
**layer** | **int** | layer integer | 
**part** | **int** | part integer | 
**color** | **int** | color integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


