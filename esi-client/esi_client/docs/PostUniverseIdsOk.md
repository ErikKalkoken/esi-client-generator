# PostUniverseIdsOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agents** | [**[PostUniverseIdsAgent]**](PostUniverseIdsAgent.md) | agents array | [optional] 
**alliances** | [**[PostUniverseIdsAlliance]**](PostUniverseIdsAlliance.md) | alliances array | [optional] 
**characters** | [**[PostUniverseIdsCharacter]**](PostUniverseIdsCharacter.md) | characters array | [optional] 
**constellations** | [**[PostUniverseIdsConstellation]**](PostUniverseIdsConstellation.md) | constellations array | [optional] 
**corporations** | [**[PostUniverseIdsCorporation]**](PostUniverseIdsCorporation.md) | corporations array | [optional] 
**factions** | [**[PostUniverseIdsFaction]**](PostUniverseIdsFaction.md) | factions array | [optional] 
**inventory_types** | [**[PostUniverseIdsInventoryType]**](PostUniverseIdsInventoryType.md) | inventory_types array | [optional] 
**regions** | [**[PostUniverseIdsRegion]**](PostUniverseIdsRegion.md) | regions array | [optional] 
**stations** | [**[PostUniverseIdsStation]**](PostUniverseIdsStation.md) | stations array | [optional] 
**systems** | [**[PostUniverseIdsSystem]**](PostUniverseIdsSystem.md) | systems array | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


