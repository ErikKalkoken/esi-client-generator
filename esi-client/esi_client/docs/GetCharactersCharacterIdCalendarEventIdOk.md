# GetCharactersCharacterIdCalendarEventIdOk

Full details of a specific event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **datetime** | date string | 
**duration** | **int** | Length in minutes | 
**event_id** | **int** | event_id integer | 
**importance** | **int** | importance integer | 
**owner_id** | **int** | owner_id integer | 
**owner_name** | **str** | owner_name string | 
**owner_type** | **str** | owner_type string | 
**response** | **str** | response string | 
**text** | **str** | text string | 
**title** | **str** | title string | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


