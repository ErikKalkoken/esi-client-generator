# GetCorporationsCorporationIdFwStatsKills

Summary of kills done by the given corporation against enemy factions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_week** | **int** | Last week&#39;s total number of kills by members of the given corporation against enemy factions | 
**total** | **int** | Total number of kills by members of the given corporation against enemy factions since the corporation enlisted | 
**yesterday** | **int** | Yesterday&#39;s total number of kills by members of the given corporation against enemy factions | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


