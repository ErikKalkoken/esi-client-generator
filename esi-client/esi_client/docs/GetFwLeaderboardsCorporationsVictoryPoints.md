# GetFwLeaderboardsCorporationsVictoryPoints

Top 10 rankings of corporations by victory points from yesterday, last week and in total

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_total** | [**[GetFwLeaderboardsCorporationsActiveTotalActiveTotal1]**](GetFwLeaderboardsCorporationsActiveTotalActiveTotal1.md) | Top 10 ranking of corporations active in faction warfare by total victory points. A corporation is considered \&quot;active\&quot; if they have participated in faction warfare in the past 14 days | 
**last_week** | [**[GetFwLeaderboardsCorporationsLastWeekLastWeek1]**](GetFwLeaderboardsCorporationsLastWeekLastWeek1.md) | Top 10 ranking of corporations by victory points in the past week | 
**yesterday** | [**[GetFwLeaderboardsCorporationsYesterdayYesterday1]**](GetFwLeaderboardsCorporationsYesterdayYesterday1.md) | Top 10 ranking of corporations by victory points in the past day | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


