# GetUniverseStationsStationIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_dockable_ship_volume** | **float** | max_dockable_ship_volume number | 
**name** | **str** | name string | 
**office_rental_cost** | **float** | office_rental_cost number | 
**position** | [**GetUniverseStationsStationIdPosition**](GetUniverseStationsStationIdPosition.md) |  | 
**reprocessing_efficiency** | **float** | reprocessing_efficiency number | 
**reprocessing_stations_take** | **float** | reprocessing_stations_take number | 
**services** | **[str]** | services array | 
**station_id** | **int** | station_id integer | 
**system_id** | **int** | The solar system this station is in | 
**type_id** | **int** | type_id integer | 
**owner** | **int** | ID of the corporation that controls this station | [optional] 
**race_id** | **int** | race_id integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


