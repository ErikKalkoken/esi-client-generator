# PostCharactersCharacterIdFittingsItem

item object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**flag** | **str** | Fitting location for the item. Entries placed in &#39;Invalid&#39; will be discarded. If this leaves the fitting with nothing, it will cause an error. | 
**quantity** | **int** | quantity integer | 
**type_id** | **int** | type_id integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


