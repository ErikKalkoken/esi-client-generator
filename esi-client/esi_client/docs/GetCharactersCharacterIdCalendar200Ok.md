# GetCharactersCharacterIdCalendar200Ok

event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_date** | **datetime** | event_date string | [optional] 
**event_id** | **int** | event_id integer | [optional] 
**event_response** | **str** | event_response string | [optional] 
**importance** | **int** | importance integer | [optional] 
**title** | **str** | title string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


