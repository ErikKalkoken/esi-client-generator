# GetCorporationsCorporationIdMedalsIssued200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character_id** | **int** | ID of the character who was rewarded this medal | 
**issued_at** | **datetime** | issued_at string | 
**issuer_id** | **int** | ID of the character who issued the medal | 
**medal_id** | **int** | medal_id integer | 
**reason** | **str** | reason string | 
**status** | **str** | status string | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


