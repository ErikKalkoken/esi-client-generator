# GetCharactersCharacterIdClonesHomeLocation

home_location object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location_id** | **int** | location_id integer | [optional] 
**location_type** | **str** | location_type string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


