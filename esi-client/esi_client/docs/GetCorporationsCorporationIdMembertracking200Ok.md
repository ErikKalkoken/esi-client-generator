# GetCorporationsCorporationIdMembertracking200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character_id** | **int** | character_id integer | 
**base_id** | **int** | base_id integer | [optional] 
**location_id** | **int** | location_id integer | [optional] 
**logoff_date** | **datetime** | logoff_date string | [optional] 
**logon_date** | **datetime** | logon_date string | [optional] 
**ship_type_id** | **int** | ship_type_id integer | [optional] 
**start_date** | **datetime** | start_date string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


