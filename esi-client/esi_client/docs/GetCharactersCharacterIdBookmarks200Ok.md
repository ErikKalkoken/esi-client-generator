# GetCharactersCharacterIdBookmarks200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookmark_id** | **int** | bookmark_id integer | 
**created** | **datetime** | created string | 
**creator_id** | **int** | creator_id integer | 
**label** | **str** | label string | 
**location_id** | **int** | location_id integer | 
**notes** | **str** | notes string | 
**coordinates** | [**GetCharactersCharacterIdBookmarksCoordinates**](GetCharactersCharacterIdBookmarksCoordinates.md) |  | [optional] 
**folder_id** | **int** | folder_id integer | [optional] 
**item** | [**GetCharactersCharacterIdBookmarksItem**](GetCharactersCharacterIdBookmarksItem.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


