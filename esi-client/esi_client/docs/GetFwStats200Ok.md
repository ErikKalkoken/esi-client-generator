# GetFwStats200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**faction_id** | **int** | faction_id integer | 
**kills** | [**GetFwStatsKills**](GetFwStatsKills.md) |  | 
**pilots** | **int** | How many pilots fight for the given faction | 
**systems_controlled** | **int** | The number of solar systems controlled by the given faction | 
**victory_points** | [**GetFwStatsVictoryPoints**](GetFwStatsVictoryPoints.md) |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


