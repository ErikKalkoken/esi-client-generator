# GetCharactersCharacterIdLocationOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**solar_system_id** | **int** | solar_system_id integer | 
**station_id** | **int** | station_id integer | [optional] 
**structure_id** | **int** | structure_id integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


