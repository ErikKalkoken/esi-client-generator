# GetCharactersCharacterIdCalendarEventIdAttendees200Ok

character_id and response of an attendee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character_id** | **int** | character_id integer | [optional] 
**event_response** | **str** | event_response string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


