# GetCorporationsCorporationIdFwStatsVictoryPoints

Summary of victory points gained by the given corporation for the enlisted faction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_week** | **int** | Last week&#39;s victory points gained by members of the given corporation | 
**total** | **int** | Total victory points gained since the given corporation enlisted | 
**yesterday** | **int** | Yesterday&#39;s victory points gained by members of the given corporation | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


