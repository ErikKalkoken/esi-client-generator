# GetUniverseBloodlines200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bloodline_id** | **int** | bloodline_id integer | 
**charisma** | **int** | charisma integer | 
**corporation_id** | **int** | corporation_id integer | 
**description** | **str** | description string | 
**intelligence** | **int** | intelligence integer | 
**memory** | **int** | memory integer | 
**name** | **str** | name string | 
**perception** | **int** | perception integer | 
**race_id** | **int** | race_id integer | 
**ship_type_id** | **int, none_type** | ship_type_id integer | 
**willpower** | **int** | willpower integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


