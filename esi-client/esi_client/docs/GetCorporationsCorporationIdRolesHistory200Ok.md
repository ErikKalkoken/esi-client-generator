# GetCorporationsCorporationIdRolesHistory200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**changed_at** | **datetime** | changed_at string | 
**character_id** | **int** | The character whose roles are changed | 
**issuer_id** | **int** | ID of the character who issued this change | 
**new_roles** | **[str]** | new_roles array | 
**old_roles** | **[str]** | old_roles array | 
**role_type** | **str** | role_type string | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


