# GetCharactersCharacterIdNotifications200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notification_id** | **int** | notification_id integer | 
**sender_id** | **int** | sender_id integer | 
**sender_type** | **str** | sender_type string | 
**timestamp** | **datetime** | timestamp string | 
**type** | **str** | type string | 
**is_read** | **bool** | is_read boolean | [optional] 
**text** | **str** | text string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


