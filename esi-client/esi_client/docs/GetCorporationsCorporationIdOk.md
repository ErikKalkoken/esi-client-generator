# GetCorporationsCorporationIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ceo_id** | **int** | ceo_id integer | 
**creator_id** | **int** | creator_id integer | 
**member_count** | **int** | member_count integer | 
**name** | **str** | the full name of the corporation | 
**tax_rate** | **float** | tax_rate number | 
**ticker** | **str** | the short name of the corporation | 
**alliance_id** | **int** | ID of the alliance that corporation is a member of, if any | [optional] 
**date_founded** | **datetime** | date_founded string | [optional] 
**description** | **str** | description string | [optional] 
**faction_id** | **int** | faction_id integer | [optional] 
**home_station_id** | **int** | home_station_id integer | [optional] 
**shares** | **int** | shares integer | [optional] 
**url** | **str** | url string | [optional] 
**war_eligible** | **bool** | war_eligible boolean | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


