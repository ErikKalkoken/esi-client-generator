# GetKillmailsKillmailIdKillmailHashAttacker

attacker object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**damage_done** | **int** | damage_done integer | 
**final_blow** | **bool** | Was the attacker the one to achieve the final blow  | 
**security_status** | **float** | Security status for the attacker  | 
**alliance_id** | **int** | alliance_id integer | [optional] 
**character_id** | **int** | character_id integer | [optional] 
**corporation_id** | **int** | corporation_id integer | [optional] 
**faction_id** | **int** | faction_id integer | [optional] 
**ship_type_id** | **int** | What ship was the attacker flying  | [optional] 
**weapon_type_id** | **int** | What weapon was used by the attacker for the kill  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


