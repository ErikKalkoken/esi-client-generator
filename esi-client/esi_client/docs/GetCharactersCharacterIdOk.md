# GetCharactersCharacterIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**birthday** | **datetime** | Creation date of the character | 
**bloodline_id** | **int** | bloodline_id integer | 
**corporation_id** | **int** | The character&#39;s corporation ID | 
**gender** | **str** | gender string | 
**name** | **str** | name string | 
**race_id** | **int** | race_id integer | 
**alliance_id** | **int** | The character&#39;s alliance ID | [optional] 
**description** | **str** | description string | [optional] 
**faction_id** | **int** | ID of the faction the character is fighting for, if the character is enlisted in Factional Warfare | [optional] 
**security_status** | **float** | security_status number | [optional] 
**title** | **str** | The individual title of the character | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


