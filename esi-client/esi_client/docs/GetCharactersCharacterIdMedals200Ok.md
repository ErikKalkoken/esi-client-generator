# GetCharactersCharacterIdMedals200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**corporation_id** | **int** | corporation_id integer | 
**date** | **datetime** | date string | 
**description** | **str** | description string | 
**graphics** | [**[GetCharactersCharacterIdMedalsGraphic]**](GetCharactersCharacterIdMedalsGraphic.md) | graphics array | 
**issuer_id** | **int** | issuer_id integer | 
**medal_id** | **int** | medal_id integer | 
**reason** | **str** | reason string | 
**status** | **str** | status string | 
**title** | **str** | title string | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


