# GetCorporationsCorporationIdAssets200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_singleton** | **bool** | is_singleton boolean | 
**item_id** | **int** | item_id integer | 
**location_flag** | **str** | location_flag string | 
**location_id** | **int** | location_id integer | 
**location_type** | **str** | location_type string | 
**quantity** | **int** | quantity integer | 
**type_id** | **int** | type_id integer | 
**is_blueprint_copy** | **bool** | is_blueprint_copy boolean | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


