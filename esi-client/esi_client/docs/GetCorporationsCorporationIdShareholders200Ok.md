# GetCorporationsCorporationIdShareholders200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**share_count** | **int** | share_count integer | 
**shareholder_id** | **int** | shareholder_id integer | 
**shareholder_type** | **str** | shareholder_type string | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


