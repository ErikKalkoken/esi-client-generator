# GetKillmailsKillmailIdKillmailHashItem

item object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**flag** | **int** | Flag for the location of the item  | 
**item_type_id** | **int** | item_type_id integer | 
**singleton** | **int** | singleton integer | 
**items** | [**[GetKillmailsKillmailIdKillmailHashItemsItem]**](GetKillmailsKillmailIdKillmailHashItemsItem.md) | items array | [optional] 
**quantity_destroyed** | **int** | How many of the item were destroyed if any  | [optional] 
**quantity_dropped** | **int** | How many of the item were dropped if any  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


