# GetDogmaEffectsEffectIdModifier

modifier object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**func** | **str** | func string | 
**domain** | **str** | domain string | [optional] 
**effect_id** | **int** | effect_id integer | [optional] 
**modified_attribute_id** | **int** | modified_attribute_id integer | [optional] 
**modifying_attribute_id** | **int** | modifying_attribute_id integer | [optional] 
**operator** | **int** | operator integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


