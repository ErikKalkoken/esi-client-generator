# GetKillmailsKillmailIdKillmailHashOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attackers** | [**[GetKillmailsKillmailIdKillmailHashAttacker]**](GetKillmailsKillmailIdKillmailHashAttacker.md) | attackers array | 
**killmail_id** | **int** | ID of the killmail | 
**killmail_time** | **datetime** | Time that the victim was killed and the killmail generated  | 
**solar_system_id** | **int** | Solar system that the kill took place in  | 
**victim** | [**GetKillmailsKillmailIdKillmailHashVictim**](GetKillmailsKillmailIdKillmailHashVictim.md) |  | 
**moon_id** | **int** | Moon if the kill took place at one | [optional] 
**war_id** | **int** | War if the killmail is generated in relation to an official war  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


