# GetCharactersCharacterIdFwStatsOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kills** | [**GetCharactersCharacterIdFwStatsKills**](GetCharactersCharacterIdFwStatsKills.md) |  | 
**victory_points** | [**GetCharactersCharacterIdFwStatsVictoryPoints**](GetCharactersCharacterIdFwStatsVictoryPoints.md) |  | 
**current_rank** | **int** | The given character&#39;s current faction rank | [optional] 
**enlisted_on** | **datetime** | The enlistment date of the given character into faction warfare. Will not be included if character is not enlisted in faction warfare | [optional] 
**faction_id** | **int** | The faction the given character is enlisted to fight for. Will not be included if character is not enlisted in faction warfare | [optional] 
**highest_rank** | **int** | The given character&#39;s highest faction rank achieved | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


