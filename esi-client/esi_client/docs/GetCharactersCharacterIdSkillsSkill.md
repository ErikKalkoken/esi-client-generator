# GetCharactersCharacterIdSkillsSkill

skill object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_skill_level** | **int** | active_skill_level integer | 
**skill_id** | **int** | skill_id integer | 
**skillpoints_in_skill** | **int** | skillpoints_in_skill integer | 
**trained_skill_level** | **int** | trained_skill_level integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


