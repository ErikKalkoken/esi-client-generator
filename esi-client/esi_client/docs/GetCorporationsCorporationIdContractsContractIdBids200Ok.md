# GetCorporationsCorporationIdContractsContractIdBids200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** | The amount bid, in ISK | 
**bid_id** | **int** | Unique ID for the bid | 
**bidder_id** | **int** | Character ID of the bidder | 
**date_bid** | **datetime** | Datetime when the bid was placed | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


