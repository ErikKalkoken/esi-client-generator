# GetCharactersCharacterIdSkillqueue200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**finished_level** | **int** | finished_level integer | 
**queue_position** | **int** | queue_position integer | 
**skill_id** | **int** | skill_id integer | 
**finish_date** | **datetime** | Date on which training of the skill will complete. Omitted if the skill queue is paused. | [optional] 
**level_end_sp** | **int** | level_end_sp integer | [optional] 
**level_start_sp** | **int** | Amount of SP that was in the skill when it started training it&#39;s current level. Used to calculate % of current level complete. | [optional] 
**start_date** | **datetime** | start_date string | [optional] 
**training_start_sp** | **int** | training_start_sp integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


