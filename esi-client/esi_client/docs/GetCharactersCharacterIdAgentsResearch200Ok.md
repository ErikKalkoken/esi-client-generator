# GetCharactersCharacterIdAgentsResearch200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent_id** | **int** | agent_id integer | 
**points_per_day** | **float** | points_per_day number | 
**remainder_points** | **float** | remainder_points number | 
**skill_type_id** | **int** | skill_type_id integer | 
**started_at** | **datetime** | started_at string | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


