# GetUniverseMoonsMoonIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**moon_id** | **int** | moon_id integer | 
**name** | **str** | name string | 
**position** | [**GetUniverseMoonsMoonIdPosition**](GetUniverseMoonsMoonIdPosition.md) |  | 
**system_id** | **int** | The solar system this moon is in | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


