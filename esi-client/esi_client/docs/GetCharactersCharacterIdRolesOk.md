# GetCharactersCharacterIdRolesOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roles** | **[str]** | roles array | [optional] 
**roles_at_base** | **[str]** | roles_at_base array | [optional] 
**roles_at_hq** | **[str]** | roles_at_hq array | [optional] 
**roles_at_other** | **[str]** | roles_at_other array | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


