# GetFwLeaderboardsKills

Top 4 rankings of factions by number of kills from yesterday, last week and in total

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_total** | [**[GetFwLeaderboardsActiveTotalActiveTotal]**](GetFwLeaderboardsActiveTotalActiveTotal.md) | Top 4 ranking of factions active in faction warfare by total kills. A faction is considered \&quot;active\&quot; if they have participated in faction warfare in the past 14 days | 
**last_week** | [**[GetFwLeaderboardsLastWeekLastWeek]**](GetFwLeaderboardsLastWeekLastWeek.md) | Top 4 ranking of factions by kills in the past week | 
**yesterday** | [**[GetFwLeaderboardsYesterdayYesterday]**](GetFwLeaderboardsYesterdayYesterday.md) | Top 4 ranking of factions by kills in the past day | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


