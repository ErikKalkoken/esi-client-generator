# GetCharactersCharacterIdPlanetsPlanetIdFactoryDetails

factory_details object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schematic_id** | **int** | schematic_id integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


