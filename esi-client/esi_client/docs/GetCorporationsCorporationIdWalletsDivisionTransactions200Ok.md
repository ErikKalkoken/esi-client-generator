# GetCorporationsCorporationIdWalletsDivisionTransactions200Ok

wallet transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **int** | client_id integer | 
**date** | **datetime** | Date and time of transaction | 
**is_buy** | **bool** | is_buy boolean | 
**journal_ref_id** | **int** | -1 if there is no corresponding wallet journal entry | 
**location_id** | **int** | location_id integer | 
**quantity** | **int** | quantity integer | 
**transaction_id** | **int** | Unique transaction ID | 
**type_id** | **int** | type_id integer | 
**unit_price** | **float** | Amount paid per unit | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


