# GetUniverseStarsStarIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age** | **int** | Age of star in years | 
**luminosity** | **float** | luminosity number | 
**name** | **str** | name string | 
**radius** | **int** | radius integer | 
**solar_system_id** | **int** | solar_system_id integer | 
**spectral_class** | **str** | spectral_class string | 
**temperature** | **int** | temperature integer | 
**type_id** | **int** | type_id integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


