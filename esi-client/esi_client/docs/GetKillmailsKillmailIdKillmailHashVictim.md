# GetKillmailsKillmailIdKillmailHashVictim

victim object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**damage_taken** | **int** | How much total damage was taken by the victim  | 
**ship_type_id** | **int** | The ship that the victim was piloting and was destroyed  | 
**alliance_id** | **int** | alliance_id integer | [optional] 
**character_id** | **int** | character_id integer | [optional] 
**corporation_id** | **int** | corporation_id integer | [optional] 
**faction_id** | **int** | faction_id integer | [optional] 
**items** | [**[GetKillmailsKillmailIdKillmailHashItem]**](GetKillmailsKillmailIdKillmailHashItem.md) | items array | [optional] 
**position** | [**GetKillmailsKillmailIdKillmailHashPosition**](GetKillmailsKillmailIdKillmailHashPosition.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


