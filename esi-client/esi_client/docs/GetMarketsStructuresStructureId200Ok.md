# GetMarketsStructuresStructureId200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **int** | duration integer | 
**is_buy_order** | **bool** | is_buy_order boolean | 
**issued** | **datetime** | issued string | 
**location_id** | **int** | location_id integer | 
**min_volume** | **int** | min_volume integer | 
**order_id** | **int** | order_id integer | 
**price** | **float** | price number | 
**range** | **str** | range string | 
**type_id** | **int** | type_id integer | 
**volume_remain** | **int** | volume_remain integer | 
**volume_total** | **int** | volume_total integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


