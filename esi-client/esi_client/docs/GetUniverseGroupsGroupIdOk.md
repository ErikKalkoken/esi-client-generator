# GetUniverseGroupsGroupIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_id** | **int** | category_id integer | 
**group_id** | **int** | group_id integer | 
**name** | **str** | name string | 
**published** | **bool** | published boolean | 
**types** | **[int]** | types array | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


