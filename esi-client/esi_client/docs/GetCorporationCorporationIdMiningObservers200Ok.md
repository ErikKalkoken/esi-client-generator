# GetCorporationCorporationIdMiningObservers200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_updated** | **date** | last_updated string | 
**observer_id** | **int** | The entity that was observing the asteroid field when it was mined.  | 
**observer_type** | **str** | The category of the observing entity | defaults to "structure"
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


