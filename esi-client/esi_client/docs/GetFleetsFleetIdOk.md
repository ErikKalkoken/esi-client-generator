# GetFleetsFleetIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_free_move** | **bool** | Is free-move enabled | 
**is_registered** | **bool** | Does the fleet have an active fleet advertisement | 
**is_voice_enabled** | **bool** | Is EVE Voice enabled | 
**motd** | **str** | Fleet MOTD in CCP flavoured HTML | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


