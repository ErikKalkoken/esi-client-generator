# PostUiOpenwindowNewmailNewMail

new_mail object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **str** | body string | 
**recipients** | **[int]** | recipients array | 
**subject** | **str** | subject string | 
**to_corp_or_alliance_id** | **int** | to_corp_or_alliance_id integer | [optional] 
**to_mailing_list_id** | **int** | Corporations, alliances and mailing lists are all types of mailing groups. You may only send to one mailing group, at a time, so you may fill out either this field or the to_corp_or_alliance_ids field | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


