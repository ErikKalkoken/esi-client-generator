# PutCharactersCharacterIdMailMailIdContents

contents object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**labels** | **[int]** | Labels to assign to the mail. Pre-existing labels are unassigned. | [optional] 
**read** | **bool** | Whether the mail is flagged as read | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


