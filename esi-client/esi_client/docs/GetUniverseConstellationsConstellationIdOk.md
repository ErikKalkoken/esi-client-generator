# GetUniverseConstellationsConstellationIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**constellation_id** | **int** | constellation_id integer | 
**name** | **str** | name string | 
**position** | [**GetUniverseConstellationsConstellationIdPosition**](GetUniverseConstellationsConstellationIdPosition.md) |  | 
**region_id** | **int** | The region this constellation is in | 
**systems** | **[int]** | systems array | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


