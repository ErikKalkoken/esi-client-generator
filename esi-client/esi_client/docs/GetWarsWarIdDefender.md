# GetWarsWarIdDefender

The defending corporation or alliance that declared this war, only contains either corporation_id or alliance_id

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isk_destroyed** | **float** | ISK value of ships the defender has killed | 
**ships_killed** | **int** | The number of ships the defender has killed | 
**alliance_id** | **int** | Alliance ID if and only if the defender is an alliance | [optional] 
**corporation_id** | **int** | Corporation ID if and only if the defender is a corporation | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


