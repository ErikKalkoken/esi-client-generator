# GetCharactersCharacterIdPlanetsPlanetIdRoute

route object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_type_id** | **int** | content_type_id integer | 
**destination_pin_id** | **int** | destination_pin_id integer | 
**quantity** | **float** | quantity number | 
**route_id** | **int** | route_id integer | 
**source_pin_id** | **int** | source_pin_id integer | 
**waypoints** | **[int]** | list of pin ID waypoints | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


