# GetUniverseStargatesStargateIdDestination

destination object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stargate_id** | **int** | The stargate this stargate connects to | 
**system_id** | **int** | The solar system this stargate connects to | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


