# GetUniverseFactions200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | description string | 
**faction_id** | **int** | faction_id integer | 
**is_unique** | **bool** | is_unique boolean | 
**name** | **str** | name string | 
**size_factor** | **float** | size_factor number | 
**station_count** | **int** | station_count integer | 
**station_system_count** | **int** | station_system_count integer | 
**corporation_id** | **int** | corporation_id integer | [optional] 
**militia_corporation_id** | **int** | militia_corporation_id integer | [optional] 
**solar_system_id** | **int** | solar_system_id integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


