# GetUniverseSystemsSystemIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**constellation_id** | **int** | The constellation this solar system is in | 
**name** | **str** | name string | 
**position** | [**GetUniverseSystemsSystemIdPosition**](GetUniverseSystemsSystemIdPosition.md) |  | 
**security_status** | **float** | security_status number | 
**system_id** | **int** | system_id integer | 
**planets** | [**[GetUniverseSystemsSystemIdPlanet]**](GetUniverseSystemsSystemIdPlanet.md) | planets array | [optional] 
**security_class** | **str** | security_class string | [optional] 
**star_id** | **int** | star_id integer | [optional] 
**stargates** | **[int]** | stargates array | [optional] 
**stations** | **[int]** | stations array | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


