# GetCharactersCharacterIdPortraitOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**px128x128** | **str** | px128x128 string | [optional] 
**px256x256** | **str** | px256x256 string | [optional] 
**px512x512** | **str** | px512x512 string | [optional] 
**px64x64** | **str** | px64x64 string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


