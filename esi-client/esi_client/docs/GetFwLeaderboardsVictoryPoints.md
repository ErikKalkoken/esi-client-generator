# GetFwLeaderboardsVictoryPoints

Top 4 rankings of factions by victory points from yesterday, last week and in total

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_total** | [**[GetFwLeaderboardsActiveTotalActiveTotal1]**](GetFwLeaderboardsActiveTotalActiveTotal1.md) | Top 4 ranking of factions active in faction warfare by total victory points. A faction is considered \&quot;active\&quot; if they have participated in faction warfare in the past 14 days | 
**last_week** | [**[GetFwLeaderboardsLastWeekLastWeek1]**](GetFwLeaderboardsLastWeekLastWeek1.md) | Top 4 ranking of factions by victory points in the past week | 
**yesterday** | [**[GetFwLeaderboardsYesterdayYesterday1]**](GetFwLeaderboardsYesterdayYesterday1.md) | Top 4 ranking of factions by victory points in the past day | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


