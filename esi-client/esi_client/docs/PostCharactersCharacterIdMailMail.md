# PostCharactersCharacterIdMailMail

mail object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **str** | body string | 
**recipients** | [**[PostCharactersCharacterIdMailRecipient]**](PostCharactersCharacterIdMailRecipient.md) | recipients array | 
**subject** | **str** | subject string | 
**approved_cost** | **int** | approved_cost integer | [optional]  if omitted the server will use the default value of 0
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


