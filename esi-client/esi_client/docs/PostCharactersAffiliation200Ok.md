# PostCharactersAffiliation200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character_id** | **int** | The character&#39;s ID | 
**corporation_id** | **int** | The character&#39;s corporation ID | 
**alliance_id** | **int** | The character&#39;s alliance ID, if their corporation is in an alliance | [optional] 
**faction_id** | **int** | The character&#39;s faction ID, if their corporation is in a faction | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


