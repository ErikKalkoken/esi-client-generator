# GetFwLeaderboardsCharactersKills

Top 100 rankings of pilots by number of kills from yesterday, last week and in total

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_total** | [**[GetFwLeaderboardsCharactersActiveTotalActiveTotal]**](GetFwLeaderboardsCharactersActiveTotalActiveTotal.md) | Top 100 ranking of pilots active in faction warfare by total kills. A pilot is considered \&quot;active\&quot; if they have participated in faction warfare in the past 14 days | 
**last_week** | [**[GetFwLeaderboardsCharactersLastWeekLastWeek]**](GetFwLeaderboardsCharactersLastWeekLastWeek.md) | Top 100 ranking of pilots by kills in the past week | 
**yesterday** | [**[GetFwLeaderboardsCharactersYesterdayYesterday]**](GetFwLeaderboardsCharactersYesterdayYesterday.md) | Top 100 ranking of pilots by kills in the past day | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


