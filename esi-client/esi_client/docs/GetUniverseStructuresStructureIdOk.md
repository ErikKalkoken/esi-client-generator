# GetUniverseStructuresStructureIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The full name of the structure | 
**owner_id** | **int** | The ID of the corporation who owns this particular structure | 
**solar_system_id** | **int** | solar_system_id integer | 
**position** | [**GetUniverseStructuresStructureIdPosition**](GetUniverseStructuresStructureIdPosition.md) |  | [optional] 
**type_id** | **int** | type_id integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


