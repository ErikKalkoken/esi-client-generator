# GetOpportunitiesGroupsGroupIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connected_groups** | **[int]** | The groups that are connected to this group on the opportunities map | 
**description** | **str** | description string | 
**group_id** | **int** | group_id integer | 
**name** | **str** | name string | 
**notification** | **str** | notification string | 
**required_tasks** | **[int]** | Tasks need to complete for this group | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


