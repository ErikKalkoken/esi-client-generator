# GetUniverseStargatesStargateIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destination** | [**GetUniverseStargatesStargateIdDestination**](GetUniverseStargatesStargateIdDestination.md) |  | 
**name** | **str** | name string | 
**position** | [**GetUniverseStargatesStargateIdPosition**](GetUniverseStargatesStargateIdPosition.md) |  | 
**stargate_id** | **int** | stargate_id integer | 
**system_id** | **int** | The solar system this stargate is in | 
**type_id** | **int** | type_id integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


