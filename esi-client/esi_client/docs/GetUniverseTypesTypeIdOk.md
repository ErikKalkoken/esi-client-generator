# GetUniverseTypesTypeIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | description string | 
**group_id** | **int** | group_id integer | 
**name** | **str** | name string | 
**published** | **bool** | published boolean | 
**type_id** | **int** | type_id integer | 
**capacity** | **float** | capacity number | [optional] 
**dogma_attributes** | [**[GetUniverseTypesTypeIdDogmaAttribute]**](GetUniverseTypesTypeIdDogmaAttribute.md) | dogma_attributes array | [optional] 
**dogma_effects** | [**[GetUniverseTypesTypeIdDogmaEffect]**](GetUniverseTypesTypeIdDogmaEffect.md) | dogma_effects array | [optional] 
**graphic_id** | **int** | graphic_id integer | [optional] 
**icon_id** | **int** | icon_id integer | [optional] 
**market_group_id** | **int** | This only exists for types that can be put on the market | [optional] 
**mass** | **float** | mass number | [optional] 
**packaged_volume** | **float** | packaged_volume number | [optional] 
**portion_size** | **int** | portion_size integer | [optional] 
**radius** | **float** | radius number | [optional] 
**volume** | **float** | volume number | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


