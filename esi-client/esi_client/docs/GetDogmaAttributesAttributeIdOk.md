# GetDogmaAttributesAttributeIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute_id** | **int** | attribute_id integer | 
**default_value** | **float** | default_value number | [optional] 
**description** | **str** | description string | [optional] 
**display_name** | **str** | display_name string | [optional] 
**high_is_good** | **bool** | high_is_good boolean | [optional] 
**icon_id** | **int** | icon_id integer | [optional] 
**name** | **str** | name string | [optional] 
**published** | **bool** | published boolean | [optional] 
**stackable** | **bool** | stackable boolean | [optional] 
**unit_id** | **int** | unit_id integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


