# GetIndustryFacilities200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**facility_id** | **int** | ID of the facility | 
**owner_id** | **int** | Owner of the facility | 
**region_id** | **int** | Region ID where the facility is | 
**solar_system_id** | **int** | Solar system ID where the facility is | 
**type_id** | **int** | Type ID of the facility | 
**tax** | **float** | Tax imposed by the facility | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


