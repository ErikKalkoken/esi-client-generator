# GetCharactersCharacterIdSkillsOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**skills** | [**[GetCharactersCharacterIdSkillsSkill]**](GetCharactersCharacterIdSkillsSkill.md) | skills array | 
**total_sp** | **int** | total_sp integer | 
**unallocated_sp** | **int** | Skill points available to be assigned | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


