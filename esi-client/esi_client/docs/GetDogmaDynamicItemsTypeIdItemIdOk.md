# GetDogmaDynamicItemsTypeIdItemIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_by** | **int** | The ID of the character who created the item | 
**dogma_attributes** | [**[GetDogmaDynamicItemsTypeIdItemIdDogmaAttribute]**](GetDogmaDynamicItemsTypeIdItemIdDogmaAttribute.md) | dogma_attributes array | 
**dogma_effects** | [**[GetDogmaDynamicItemsTypeIdItemIdDogmaEffect]**](GetDogmaDynamicItemsTypeIdItemIdDogmaEffect.md) | dogma_effects array | 
**mutator_type_id** | **int** | The type ID of the mutator used to generate the dynamic item. | 
**source_type_id** | **int** | The type ID of the source item the mutator was applied to create the dynamic item. | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


