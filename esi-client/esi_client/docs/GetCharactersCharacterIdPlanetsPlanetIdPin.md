# GetCharactersCharacterIdPlanetsPlanetIdPin

pin object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **float** | latitude number | 
**longitude** | **float** | longitude number | 
**pin_id** | **int** | pin_id integer | 
**type_id** | **int** | type_id integer | 
**contents** | [**[GetCharactersCharacterIdPlanetsPlanetIdContent]**](GetCharactersCharacterIdPlanetsPlanetIdContent.md) | contents array | [optional] 
**expiry_time** | **datetime** | expiry_time string | [optional] 
**extractor_details** | [**GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails**](GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails.md) |  | [optional] 
**factory_details** | [**GetCharactersCharacterIdPlanetsPlanetIdFactoryDetails**](GetCharactersCharacterIdPlanetsPlanetIdFactoryDetails.md) |  | [optional] 
**install_time** | **datetime** | install_time string | [optional] 
**last_cycle_start** | **datetime** | last_cycle_start string | [optional] 
**schematic_id** | **int** | schematic_id integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


