# GetCharactersCharacterIdMailMailIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **str** | Mail&#39;s body | [optional] 
**_from** | **int** | From whom the mail was sent | [optional] 
**labels** | **[int]** | Labels attached to the mail | [optional] 
**read** | **bool** | Whether the mail is flagged as read | [optional] 
**recipients** | [**[GetCharactersCharacterIdMailMailIdRecipient]**](GetCharactersCharacterIdMailMailIdRecipient.md) | Recipients of the mail | [optional] 
**subject** | **str** | Mail subject | [optional] 
**timestamp** | **datetime** | When the mail was sent | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


