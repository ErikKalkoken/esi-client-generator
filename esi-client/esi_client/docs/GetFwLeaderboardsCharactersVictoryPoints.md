# GetFwLeaderboardsCharactersVictoryPoints

Top 100 rankings of pilots by victory points from yesterday, last week and in total

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_total** | [**[GetFwLeaderboardsCharactersActiveTotalActiveTotal1]**](GetFwLeaderboardsCharactersActiveTotalActiveTotal1.md) | Top 100 ranking of pilots active in faction warfare by total victory points. A pilot is considered \&quot;active\&quot; if they have participated in faction warfare in the past 14 days | 
**last_week** | [**[GetFwLeaderboardsCharactersLastWeekLastWeek1]**](GetFwLeaderboardsCharactersLastWeekLastWeek1.md) | Top 100 ranking of pilots by victory points in the past week | 
**yesterday** | [**[GetFwLeaderboardsCharactersYesterdayYesterday1]**](GetFwLeaderboardsCharactersYesterdayYesterday1.md) | Top 100 ranking of pilots by victory points in the past day | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


