# GetCharactersCharacterIdFatigueOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jump_fatigue_expire_date** | **datetime** | Character&#39;s jump fatigue expiry | [optional] 
**last_jump_date** | **datetime** | Character&#39;s last jump activation | [optional] 
**last_update_date** | **datetime** | Character&#39;s last jump update | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


