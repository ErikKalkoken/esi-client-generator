# GetUniverseAncestries200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bloodline_id** | **int** | The bloodline associated with this ancestry | 
**description** | **str** | description string | 
**id** | **int** | id integer | 
**name** | **str** | name string | 
**icon_id** | **int** | icon_id integer | [optional] 
**short_description** | **str** | short_description string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


