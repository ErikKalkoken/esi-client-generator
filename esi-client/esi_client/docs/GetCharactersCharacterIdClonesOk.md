# GetCharactersCharacterIdClonesOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jump_clones** | [**[GetCharactersCharacterIdClonesJumpClone]**](GetCharactersCharacterIdClonesJumpClone.md) | jump_clones array | 
**home_location** | [**GetCharactersCharacterIdClonesHomeLocation**](GetCharactersCharacterIdClonesHomeLocation.md) |  | [optional] 
**last_clone_jump_date** | **datetime** | last_clone_jump_date string | [optional] 
**last_station_change_date** | **datetime** | last_station_change_date string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


