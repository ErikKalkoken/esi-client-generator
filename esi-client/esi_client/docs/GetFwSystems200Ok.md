# GetFwSystems200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contested** | **str** | contested string | 
**occupier_faction_id** | **int** | occupier_faction_id integer | 
**owner_faction_id** | **int** | owner_faction_id integer | 
**solar_system_id** | **int** | solar_system_id integer | 
**victory_points** | **int** | victory_points integer | 
**victory_points_threshold** | **int** | victory_points_threshold integer | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


