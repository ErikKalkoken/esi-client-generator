# GetCorporationsCorporationIdAlliancehistory200Ok

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**record_id** | **int** | An incrementing ID that can be used to canonically establish order of records in cases where dates may be ambiguous | 
**start_date** | **datetime** | start_date string | 
**alliance_id** | **int** | alliance_id integer | [optional] 
**is_deleted** | **bool** | True if the alliance has been closed | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


