# GetMarketsGroupsMarketGroupIdOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | description string | 
**market_group_id** | **int** | market_group_id integer | 
**name** | **str** | name string | 
**types** | **[int]** | types array | 
**parent_group_id** | **int** | parent_group_id integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


