# GetKillmailsKillmailIdKillmailHashItemsItem

item object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**flag** | **int** | flag integer | 
**item_type_id** | **int** | item_type_id integer | 
**singleton** | **int** | singleton integer | 
**quantity_destroyed** | **int** | quantity_destroyed integer | [optional] 
**quantity_dropped** | **int** | quantity_dropped integer | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


