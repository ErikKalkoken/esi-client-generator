# GetCharactersCharacterIdOnlineOk

200 ok object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**online** | **bool** | If the character is online | 
**last_login** | **datetime** | Timestamp of the last login | [optional] 
**last_logout** | **datetime** | Timestamp of the last logout | [optional] 
**logins** | **int** | Total number of times the character has logged in | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


