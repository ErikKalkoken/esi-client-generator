"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from esi_client.api_client import ApiClient, Endpoint as _Endpoint
from esi_client.model_utils import (  # noqa: F401
    check_allowed_values,
    check_validations,
    date,
    datetime,
    file_type,
    none_type,
    validate_and_convert_types
)
from esi_client.model.bad_request import BadRequest
from esi_client.model.error_limited import ErrorLimited
from esi_client.model.forbidden import Forbidden
from esi_client.model.gateway_timeout import GatewayTimeout
from esi_client.model.get_characters_character_id_planets200_ok import GetCharactersCharacterIdPlanets200Ok
from esi_client.model.get_characters_character_id_planets_planet_id_not_found import GetCharactersCharacterIdPlanetsPlanetIdNotFound
from esi_client.model.get_characters_character_id_planets_planet_id_ok import GetCharactersCharacterIdPlanetsPlanetIdOk
from esi_client.model.get_corporations_corporation_id_customs_offices200_ok import GetCorporationsCorporationIdCustomsOffices200Ok
from esi_client.model.get_universe_schematics_schematic_id_not_found import GetUniverseSchematicsSchematicIdNotFound
from esi_client.model.get_universe_schematics_schematic_id_ok import GetUniverseSchematicsSchematicIdOk
from esi_client.model.internal_server_error import InternalServerError
from esi_client.model.service_unavailable import ServiceUnavailable
from esi_client.model.unauthorized import Unauthorized


class PlanetaryInteractionApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client
        self.get_characters_character_id_planets_endpoint = _Endpoint(
            settings={
                'response_type': ([GetCharactersCharacterIdPlanets200Ok],),
                'auth': [
                    'evesso'
                ],
                'endpoint_path': '/characters/{character_id}/planets/',
                'operation_id': 'get_characters_character_id_planets',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'character_id',
                    'datasource',
                    'if_none_match',
                    'token',
                ],
                'required': [
                    'character_id',
                ],
                'nullable': [
                ],
                'enum': [
                    'datasource',
                ],
                'validation': [
                    'character_id',
                ]
            },
            root_map={
                'validations': {
                    ('character_id',): {

                        'inclusive_minimum': 1,
                    },
                },
                'allowed_values': {
                    ('datasource',): {

                        "TRANQUILITY": "tranquility"
                    },
                },
                'openapi_types': {
                    'character_id':
                        (int,),
                    'datasource':
                        (str,),
                    'if_none_match':
                        (str,),
                    'token':
                        (str,),
                },
                'attribute_map': {
                    'character_id': 'character_id',
                    'datasource': 'datasource',
                    'if_none_match': 'If-None-Match',
                    'token': 'token',
                },
                'location_map': {
                    'character_id': 'path',
                    'datasource': 'query',
                    'if_none_match': 'header',
                    'token': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client
        )
        self.get_characters_character_id_planets_planet_id_endpoint = _Endpoint(
            settings={
                'response_type': (GetCharactersCharacterIdPlanetsPlanetIdOk,),
                'auth': [
                    'evesso'
                ],
                'endpoint_path': '/characters/{character_id}/planets/{planet_id}/',
                'operation_id': 'get_characters_character_id_planets_planet_id',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'character_id',
                    'planet_id',
                    'datasource',
                    'token',
                ],
                'required': [
                    'character_id',
                    'planet_id',
                ],
                'nullable': [
                ],
                'enum': [
                    'datasource',
                ],
                'validation': [
                    'character_id',
                ]
            },
            root_map={
                'validations': {
                    ('character_id',): {

                        'inclusive_minimum': 1,
                    },
                },
                'allowed_values': {
                    ('datasource',): {

                        "TRANQUILITY": "tranquility"
                    },
                },
                'openapi_types': {
                    'character_id':
                        (int,),
                    'planet_id':
                        (int,),
                    'datasource':
                        (str,),
                    'token':
                        (str,),
                },
                'attribute_map': {
                    'character_id': 'character_id',
                    'planet_id': 'planet_id',
                    'datasource': 'datasource',
                    'token': 'token',
                },
                'location_map': {
                    'character_id': 'path',
                    'planet_id': 'path',
                    'datasource': 'query',
                    'token': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client
        )
        self.get_corporations_corporation_id_customs_offices_endpoint = _Endpoint(
            settings={
                'response_type': ([GetCorporationsCorporationIdCustomsOffices200Ok],),
                'auth': [
                    'evesso'
                ],
                'endpoint_path': '/corporations/{corporation_id}/customs_offices/',
                'operation_id': 'get_corporations_corporation_id_customs_offices',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'corporation_id',
                    'datasource',
                    'if_none_match',
                    'page',
                    'token',
                ],
                'required': [
                    'corporation_id',
                ],
                'nullable': [
                ],
                'enum': [
                    'datasource',
                ],
                'validation': [
                    'corporation_id',
                    'page',
                ]
            },
            root_map={
                'validations': {
                    ('corporation_id',): {

                        'inclusive_minimum': 1,
                    },
                    ('page',): {

                        'inclusive_minimum': 1,
                    },
                },
                'allowed_values': {
                    ('datasource',): {

                        "TRANQUILITY": "tranquility"
                    },
                },
                'openapi_types': {
                    'corporation_id':
                        (int,),
                    'datasource':
                        (str,),
                    'if_none_match':
                        (str,),
                    'page':
                        (int,),
                    'token':
                        (str,),
                },
                'attribute_map': {
                    'corporation_id': 'corporation_id',
                    'datasource': 'datasource',
                    'if_none_match': 'If-None-Match',
                    'page': 'page',
                    'token': 'token',
                },
                'location_map': {
                    'corporation_id': 'path',
                    'datasource': 'query',
                    'if_none_match': 'header',
                    'page': 'query',
                    'token': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client
        )
        self.get_universe_schematics_schematic_id_endpoint = _Endpoint(
            settings={
                'response_type': (GetUniverseSchematicsSchematicIdOk,),
                'auth': [],
                'endpoint_path': '/universe/schematics/{schematic_id}/',
                'operation_id': 'get_universe_schematics_schematic_id',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'schematic_id',
                    'datasource',
                    'if_none_match',
                ],
                'required': [
                    'schematic_id',
                ],
                'nullable': [
                ],
                'enum': [
                    'datasource',
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                    ('datasource',): {

                        "TRANQUILITY": "tranquility"
                    },
                },
                'openapi_types': {
                    'schematic_id':
                        (int,),
                    'datasource':
                        (str,),
                    'if_none_match':
                        (str,),
                },
                'attribute_map': {
                    'schematic_id': 'schematic_id',
                    'datasource': 'datasource',
                    'if_none_match': 'If-None-Match',
                },
                'location_map': {
                    'schematic_id': 'path',
                    'datasource': 'query',
                    'if_none_match': 'header',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client
        )

    def get_characters_character_id_planets(
        self,
        character_id,
        **kwargs
    ):
        """Get colonies  # noqa: E501

        Returns a list of all planetary colonies owned by a character.  --- Alternate route: `/dev/characters/{character_id}/planets/`  Alternate route: `/legacy/characters/{character_id}/planets/`  Alternate route: `/v1/characters/{character_id}/planets/`  --- This route is cached for up to 600 seconds  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_characters_character_id_planets(character_id, async_req=True)
        >>> result = thread.get()

        Args:
            character_id (int): An EVE character ID

        Keyword Args:
            datasource (str): The server name you would like data from. [optional] if omitted the server will use the default value of "tranquility"
            if_none_match (str): ETag from a previous request. A 304 will be returned if this matches the current ETag. [optional]
            token (str): Access token to use if unable to set a header. [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            _request_auths (list): set to override the auth_settings for an a single
                request; this effectively ignores the authentication
                in the spec for a single request.
                Default is None
            async_req (bool): execute request asynchronously

        Returns:
            [GetCharactersCharacterIdPlanets200Ok]
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['_request_auths'] = kwargs.get('_request_auths', None)
        kwargs['character_id'] = \
            character_id
        return self.get_characters_character_id_planets_endpoint.call_with_http_info(**kwargs)

    def get_characters_character_id_planets_planet_id(
        self,
        character_id,
        planet_id,
        **kwargs
    ):
        """Get colony layout  # noqa: E501

        Returns full details on the layout of a single planetary colony, including links, pins and routes. Note: Planetary information is only recalculated when the colony is viewed through the client. Information will not update until this criteria is met.  --- Alternate route: `/dev/characters/{character_id}/planets/{planet_id}/`  Alternate route: `/v3/characters/{character_id}/planets/{planet_id}/`   # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_characters_character_id_planets_planet_id(character_id, planet_id, async_req=True)
        >>> result = thread.get()

        Args:
            character_id (int): An EVE character ID
            planet_id (int): Planet id of the target planet

        Keyword Args:
            datasource (str): The server name you would like data from. [optional] if omitted the server will use the default value of "tranquility"
            token (str): Access token to use if unable to set a header. [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            _request_auths (list): set to override the auth_settings for an a single
                request; this effectively ignores the authentication
                in the spec for a single request.
                Default is None
            async_req (bool): execute request asynchronously

        Returns:
            GetCharactersCharacterIdPlanetsPlanetIdOk
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['_request_auths'] = kwargs.get('_request_auths', None)
        kwargs['character_id'] = \
            character_id
        kwargs['planet_id'] = \
            planet_id
        return self.get_characters_character_id_planets_planet_id_endpoint.call_with_http_info(**kwargs)

    def get_corporations_corporation_id_customs_offices(
        self,
        corporation_id,
        **kwargs
    ):
        """List corporation customs offices  # noqa: E501

        List customs offices owned by a corporation  --- Alternate route: `/dev/corporations/{corporation_id}/customs_offices/`  Alternate route: `/legacy/corporations/{corporation_id}/customs_offices/`  Alternate route: `/v1/corporations/{corporation_id}/customs_offices/`  --- This route is cached for up to 3600 seconds  --- Requires one of the following EVE corporation role(s): Director   # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_corporations_corporation_id_customs_offices(corporation_id, async_req=True)
        >>> result = thread.get()

        Args:
            corporation_id (int): An EVE corporation ID

        Keyword Args:
            datasource (str): The server name you would like data from. [optional] if omitted the server will use the default value of "tranquility"
            if_none_match (str): ETag from a previous request. A 304 will be returned if this matches the current ETag. [optional]
            page (int): Which page of results to return. [optional] if omitted the server will use the default value of 1
            token (str): Access token to use if unable to set a header. [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            _request_auths (list): set to override the auth_settings for an a single
                request; this effectively ignores the authentication
                in the spec for a single request.
                Default is None
            async_req (bool): execute request asynchronously

        Returns:
            [GetCorporationsCorporationIdCustomsOffices200Ok]
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['_request_auths'] = kwargs.get('_request_auths', None)
        kwargs['corporation_id'] = \
            corporation_id
        return self.get_corporations_corporation_id_customs_offices_endpoint.call_with_http_info(**kwargs)

    def get_universe_schematics_schematic_id(
        self,
        schematic_id,
        **kwargs
    ):
        """Get schematic information  # noqa: E501

        Get information on a planetary factory schematic  --- Alternate route: `/dev/universe/schematics/{schematic_id}/`  Alternate route: `/legacy/universe/schematics/{schematic_id}/`  Alternate route: `/v1/universe/schematics/{schematic_id}/`  --- This route is cached for up to 3600 seconds  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.get_universe_schematics_schematic_id(schematic_id, async_req=True)
        >>> result = thread.get()

        Args:
            schematic_id (int): A PI schematic ID

        Keyword Args:
            datasource (str): The server name you would like data from. [optional] if omitted the server will use the default value of "tranquility"
            if_none_match (str): ETag from a previous request. A 304 will be returned if this matches the current ETag. [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            _request_auths (list): set to override the auth_settings for an a single
                request; this effectively ignores the authentication
                in the spec for a single request.
                Default is None
            async_req (bool): execute request asynchronously

        Returns:
            GetUniverseSchematicsSchematicIdOk
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['_request_auths'] = kwargs.get('_request_auths', None)
        kwargs['schematic_id'] = \
            schematic_id
        return self.get_universe_schematics_schematic_id_endpoint.call_with_http_info(**kwargs)

