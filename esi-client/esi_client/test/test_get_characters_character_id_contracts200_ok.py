"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_characters_character_id_contracts200_ok import GetCharactersCharacterIdContracts200Ok


class TestGetCharactersCharacterIdContracts200Ok(unittest.TestCase):
    """GetCharactersCharacterIdContracts200Ok unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetCharactersCharacterIdContracts200Ok(self):
        """Test GetCharactersCharacterIdContracts200Ok"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetCharactersCharacterIdContracts200Ok()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
