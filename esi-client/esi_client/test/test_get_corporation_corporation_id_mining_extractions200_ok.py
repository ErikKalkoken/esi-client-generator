"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_corporation_corporation_id_mining_extractions200_ok import GetCorporationCorporationIdMiningExtractions200Ok


class TestGetCorporationCorporationIdMiningExtractions200Ok(unittest.TestCase):
    """GetCorporationCorporationIdMiningExtractions200Ok unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetCorporationCorporationIdMiningExtractions200Ok(self):
        """Test GetCorporationCorporationIdMiningExtractions200Ok"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetCorporationCorporationIdMiningExtractions200Ok()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
