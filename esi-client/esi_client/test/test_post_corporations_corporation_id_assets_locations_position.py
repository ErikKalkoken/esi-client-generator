"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.post_corporations_corporation_id_assets_locations_position import PostCorporationsCorporationIdAssetsLocationsPosition


class TestPostCorporationsCorporationIdAssetsLocationsPosition(unittest.TestCase):
    """PostCorporationsCorporationIdAssetsLocationsPosition unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPostCorporationsCorporationIdAssetsLocationsPosition(self):
        """Test PostCorporationsCorporationIdAssetsLocationsPosition"""
        # FIXME: construct object with mandatory attributes with example values
        # model = PostCorporationsCorporationIdAssetsLocationsPosition()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
