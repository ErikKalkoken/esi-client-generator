"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_characters_character_id_location_ok import GetCharactersCharacterIdLocationOk


class TestGetCharactersCharacterIdLocationOk(unittest.TestCase):
    """GetCharactersCharacterIdLocationOk unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetCharactersCharacterIdLocationOk(self):
        """Test GetCharactersCharacterIdLocationOk"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetCharactersCharacterIdLocationOk()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
