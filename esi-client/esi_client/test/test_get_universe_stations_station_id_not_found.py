"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_universe_stations_station_id_not_found import GetUniverseStationsStationIdNotFound


class TestGetUniverseStationsStationIdNotFound(unittest.TestCase):
    """GetUniverseStationsStationIdNotFound unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetUniverseStationsStationIdNotFound(self):
        """Test GetUniverseStationsStationIdNotFound"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetUniverseStationsStationIdNotFound()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
