"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import unittest

import esi_client
from esi_client.api.incursions_api import IncursionsApi  # noqa: E501


class TestIncursionsApi(unittest.TestCase):
    """IncursionsApi unit test stubs"""

    def setUp(self):
        self.api = IncursionsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_incursions(self):
        """Test case for get_incursions

        List incursions  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
