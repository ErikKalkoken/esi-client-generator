"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_contracts_public_items_contract_id200_ok import GetContractsPublicItemsContractId200Ok


class TestGetContractsPublicItemsContractId200Ok(unittest.TestCase):
    """GetContractsPublicItemsContractId200Ok unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetContractsPublicItemsContractId200Ok(self):
        """Test GetContractsPublicItemsContractId200Ok"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetContractsPublicItemsContractId200Ok()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
