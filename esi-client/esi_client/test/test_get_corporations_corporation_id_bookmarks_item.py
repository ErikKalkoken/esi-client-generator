"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_corporations_corporation_id_bookmarks_item import GetCorporationsCorporationIdBookmarksItem


class TestGetCorporationsCorporationIdBookmarksItem(unittest.TestCase):
    """GetCorporationsCorporationIdBookmarksItem unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetCorporationsCorporationIdBookmarksItem(self):
        """Test GetCorporationsCorporationIdBookmarksItem"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetCorporationsCorporationIdBookmarksItem()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
