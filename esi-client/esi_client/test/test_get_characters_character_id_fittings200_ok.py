"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_characters_character_id_fittings_item import GetCharactersCharacterIdFittingsItem
globals()['GetCharactersCharacterIdFittingsItem'] = GetCharactersCharacterIdFittingsItem
from esi_client.model.get_characters_character_id_fittings200_ok import GetCharactersCharacterIdFittings200Ok


class TestGetCharactersCharacterIdFittings200Ok(unittest.TestCase):
    """GetCharactersCharacterIdFittings200Ok unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetCharactersCharacterIdFittings200Ok(self):
        """Test GetCharactersCharacterIdFittings200Ok"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetCharactersCharacterIdFittings200Ok()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
