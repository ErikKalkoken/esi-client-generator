"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_universe_planets_planet_id_position import GetUniversePlanetsPlanetIdPosition


class TestGetUniversePlanetsPlanetIdPosition(unittest.TestCase):
    """GetUniversePlanetsPlanetIdPosition unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetUniversePlanetsPlanetIdPosition(self):
        """Test GetUniversePlanetsPlanetIdPosition"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetUniversePlanetsPlanetIdPosition()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
