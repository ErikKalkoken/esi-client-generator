"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import unittest

import esi_client
from esi_client.api.sovereignty_api import SovereigntyApi  # noqa: E501


class TestSovereigntyApi(unittest.TestCase):
    """SovereigntyApi unit test stubs"""

    def setUp(self):
        self.api = SovereigntyApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_sovereignty_campaigns(self):
        """Test case for get_sovereignty_campaigns

        List sovereignty campaigns  # noqa: E501
        """
        pass

    def test_get_sovereignty_map(self):
        """Test case for get_sovereignty_map

        List sovereignty of systems  # noqa: E501
        """
        pass

    def test_get_sovereignty_structures(self):
        """Test case for get_sovereignty_structures

        List sovereignty structures  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
