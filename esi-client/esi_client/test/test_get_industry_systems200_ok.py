"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_industry_systems_cost_indice import GetIndustrySystemsCostIndice
globals()['GetIndustrySystemsCostIndice'] = GetIndustrySystemsCostIndice
from esi_client.model.get_industry_systems200_ok import GetIndustrySystems200Ok


class TestGetIndustrySystems200Ok(unittest.TestCase):
    """GetIndustrySystems200Ok unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetIndustrySystems200Ok(self):
        """Test GetIndustrySystems200Ok"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetIndustrySystems200Ok()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
