"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.put_fleets_fleet_id_members_member_id_not_found import PutFleetsFleetIdMembersMemberIdNotFound


class TestPutFleetsFleetIdMembersMemberIdNotFound(unittest.TestCase):
    """PutFleetsFleetIdMembersMemberIdNotFound unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPutFleetsFleetIdMembersMemberIdNotFound(self):
        """Test PutFleetsFleetIdMembersMemberIdNotFound"""
        # FIXME: construct object with mandatory attributes with example values
        # model = PutFleetsFleetIdMembersMemberIdNotFound()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
