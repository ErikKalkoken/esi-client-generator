"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_opportunities_tasks_task_id_ok import GetOpportunitiesTasksTaskIdOk


class TestGetOpportunitiesTasksTaskIdOk(unittest.TestCase):
    """GetOpportunitiesTasksTaskIdOk unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetOpportunitiesTasksTaskIdOk(self):
        """Test GetOpportunitiesTasksTaskIdOk"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetOpportunitiesTasksTaskIdOk()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
