"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_corporations_corporation_id_industry_jobs200_ok import GetCorporationsCorporationIdIndustryJobs200Ok


class TestGetCorporationsCorporationIdIndustryJobs200Ok(unittest.TestCase):
    """GetCorporationsCorporationIdIndustryJobs200Ok unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetCorporationsCorporationIdIndustryJobs200Ok(self):
        """Test GetCorporationsCorporationIdIndustryJobs200Ok"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetCorporationsCorporationIdIndustryJobs200Ok()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
