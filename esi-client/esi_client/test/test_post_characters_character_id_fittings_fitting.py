"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.post_characters_character_id_fittings_item import PostCharactersCharacterIdFittingsItem
globals()['PostCharactersCharacterIdFittingsItem'] = PostCharactersCharacterIdFittingsItem
from esi_client.model.post_characters_character_id_fittings_fitting import PostCharactersCharacterIdFittingsFitting


class TestPostCharactersCharacterIdFittingsFitting(unittest.TestCase):
    """PostCharactersCharacterIdFittingsFitting unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPostCharactersCharacterIdFittingsFitting(self):
        """Test PostCharactersCharacterIdFittingsFitting"""
        # FIXME: construct object with mandatory attributes with example values
        # model = PostCharactersCharacterIdFittingsFitting()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
