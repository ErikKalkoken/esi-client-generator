"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_universe_types_type_id_dogma_attribute import GetUniverseTypesTypeIdDogmaAttribute
from esi_client.model.get_universe_types_type_id_dogma_effect import GetUniverseTypesTypeIdDogmaEffect
globals()['GetUniverseTypesTypeIdDogmaAttribute'] = GetUniverseTypesTypeIdDogmaAttribute
globals()['GetUniverseTypesTypeIdDogmaEffect'] = GetUniverseTypesTypeIdDogmaEffect
from esi_client.model.get_universe_types_type_id_ok import GetUniverseTypesTypeIdOk


class TestGetUniverseTypesTypeIdOk(unittest.TestCase):
    """GetUniverseTypesTypeIdOk unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetUniverseTypesTypeIdOk(self):
        """Test GetUniverseTypesTypeIdOk"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetUniverseTypesTypeIdOk()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
