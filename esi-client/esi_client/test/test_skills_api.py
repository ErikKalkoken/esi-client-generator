"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import unittest

import esi_client
from esi_client.api.skills_api import SkillsApi  # noqa: E501


class TestSkillsApi(unittest.TestCase):
    """SkillsApi unit test stubs"""

    def setUp(self):
        self.api = SkillsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_characters_character_id_attributes(self):
        """Test case for get_characters_character_id_attributes

        Get character attributes  # noqa: E501
        """
        pass

    def test_get_characters_character_id_skillqueue(self):
        """Test case for get_characters_character_id_skillqueue

        Get character's skill queue  # noqa: E501
        """
        pass

    def test_get_characters_character_id_skills(self):
        """Test case for get_characters_character_id_skills

        Get character skills  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
