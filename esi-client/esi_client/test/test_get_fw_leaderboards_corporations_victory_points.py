"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_fw_leaderboards_corporations_active_total_active_total1 import GetFwLeaderboardsCorporationsActiveTotalActiveTotal1
from esi_client.model.get_fw_leaderboards_corporations_last_week_last_week1 import GetFwLeaderboardsCorporationsLastWeekLastWeek1
from esi_client.model.get_fw_leaderboards_corporations_yesterday_yesterday1 import GetFwLeaderboardsCorporationsYesterdayYesterday1
globals()['GetFwLeaderboardsCorporationsActiveTotalActiveTotal1'] = GetFwLeaderboardsCorporationsActiveTotalActiveTotal1
globals()['GetFwLeaderboardsCorporationsLastWeekLastWeek1'] = GetFwLeaderboardsCorporationsLastWeekLastWeek1
globals()['GetFwLeaderboardsCorporationsYesterdayYesterday1'] = GetFwLeaderboardsCorporationsYesterdayYesterday1
from esi_client.model.get_fw_leaderboards_corporations_victory_points import GetFwLeaderboardsCorporationsVictoryPoints


class TestGetFwLeaderboardsCorporationsVictoryPoints(unittest.TestCase):
    """GetFwLeaderboardsCorporationsVictoryPoints unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetFwLeaderboardsCorporationsVictoryPoints(self):
        """Test GetFwLeaderboardsCorporationsVictoryPoints"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetFwLeaderboardsCorporationsVictoryPoints()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
