"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_characters_character_id_killmails_recent200_ok import GetCharactersCharacterIdKillmailsRecent200Ok


class TestGetCharactersCharacterIdKillmailsRecent200Ok(unittest.TestCase):
    """GetCharactersCharacterIdKillmailsRecent200Ok unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetCharactersCharacterIdKillmailsRecent200Ok(self):
        """Test GetCharactersCharacterIdKillmailsRecent200Ok"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetCharactersCharacterIdKillmailsRecent200Ok()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
