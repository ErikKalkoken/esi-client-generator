"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_killmails_killmail_id_killmail_hash_items_item import GetKillmailsKillmailIdKillmailHashItemsItem


class TestGetKillmailsKillmailIdKillmailHashItemsItem(unittest.TestCase):
    """GetKillmailsKillmailIdKillmailHashItemsItem unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetKillmailsKillmailIdKillmailHashItemsItem(self):
        """Test GetKillmailsKillmailIdKillmailHashItemsItem"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetKillmailsKillmailIdKillmailHashItemsItem()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
