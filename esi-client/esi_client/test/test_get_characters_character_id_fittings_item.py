"""
    EVE Swagger Interface

    An OpenAPI for EVE Online  # noqa: E501

    The version of the OpenAPI document: 1.28
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import esi_client
from esi_client.model.get_characters_character_id_fittings_item import GetCharactersCharacterIdFittingsItem


class TestGetCharactersCharacterIdFittingsItem(unittest.TestCase):
    """GetCharactersCharacterIdFittingsItem unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetCharactersCharacterIdFittingsItem(self):
        """Test GetCharactersCharacterIdFittingsItem"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetCharactersCharacterIdFittingsItem()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
